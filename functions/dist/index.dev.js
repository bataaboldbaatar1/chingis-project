"use strict";

var functions = require('firebase-functions');

var admin = require('firebase-admin');

admin.initializeApp();
exports.deleteEvent = functions.firestore.document("Events/{doc}").onDelete(function (snapshot) {
  // console.log(snapshot.data());
  var data = snapshot.data();
  admin.firestore().collection("users/".concat(data.createdUser, "/").concat(data.name)).onSnapshot(function (querySnapshot) {
    setData(querySnapshot.docs.map(function (doc) {
      admin.firestore().doc("users/".concat(data.createdUser, "/").concat(data.name, "/").concat(doc.id))["delete"]();
    }));
  });
});
exports.userComment = functions.firestore.document("Events/{event}/Comments/{doc}").onWrite(function (change, context) {
  // console.log(change.after.data());
  admin.firestore().collection("users/".concat(change.after.data().uid, "/Comments/")).doc("".concat(change.after.data().eventId)).set({
    comment: change.after.data().comment
  });
});
exports.imageToken = functions.storage.object().onFinalize(function _callee(object) {
  var name, token, selfLink;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          // await console.log(object);
          name = object.name;
          token = object.metadata.firebaseStorageDownloadTokens;
          selfLink = object.selfLink; // console.log(name.split('/')[1], "=======aposugiflkadjgipasfh");

          if (!(name.split('/')[0] === "Tenders")) {
            _context.next = 13;
            break;
          }

          if (!(name.split('/')[3] === 'MainImage.jpg')) {
            _context.next = 9;
            break;
          }

          _context.next = 7;
          return regeneratorRuntime.awrap(admin.firestore().collection("Tenders/").doc(name.split('/')[2]).set({
            mainImageUrl: "https://firebasestorage.googleapis.com/v0/b/funding-project.appspot.com/o/".concat(selfLink.split('/o/')[1], "?alt=media&token=").concat(token)
          }, {
            merge: true
          }));

        case 7:
          _context.next = 11;
          break;

        case 9:
          _context.next = 11;
          return regeneratorRuntime.awrap(admin.firestore().collection("Tenders/".concat(name.split('/')[2], "/SideImageUrls/")).doc().set({
            url: "https://firebasestorage.googleapis.com/v0/b/funding-project.appspot.com/o/".concat(selfLink.split('/o/')[1], "?alt=media&token=").concat(token)
          }, {
            merge: true
          }));

        case 11:
          _context.next = 23;
          break;

        case 13:
          if (!(name.split('/')[0] === "profileImages")) {
            _context.next = 16;
            break;
          }

          _context.next = 16;
          return regeneratorRuntime.awrap(admin.firestore().collection("users").doc(name.split('/')[1]).set({
            profileImageUrl: "https://firebasestorage.googleapis.com/v0/b/funding-project.appspot.com/o/".concat(selfLink.split('/o/')[1], "?alt=media&token=").concat(token)
          }, {
            merge: true
          }));

        case 16:
          if (!(name.split('/')[2] === 'MainImage.jpg')) {
            _context.next = 21;
            break;
          }

          _context.next = 19;
          return regeneratorRuntime.awrap(admin.firestore().collection("Events/").doc(name.split('/')[1]).set({
            mainImageUrl: "https://firebasestorage.googleapis.com/v0/b/funding-project.appspot.com/o/".concat(selfLink.split('/o/')[1], "?alt=media&token=").concat(token)
          }, {
            merge: true
          }));

        case 19:
          _context.next = 23;
          break;

        case 21:
          _context.next = 23;
          return regeneratorRuntime.awrap(admin.firestore().collection("Events/".concat(name.split('/')[1], "/SideImageUrls/")).doc().set({
            url: "https://firebasestorage.googleapis.com/v0/b/funding-project.appspot.com/o/".concat(selfLink.split('/o/')[1], "?alt=media&token=").concat(token)
          }, {
            merge: true
          }));

        case 23:
        case "end":
          return _context.stop();
      }
    }
  });
}); // export const sendEmailVerification = functions.auth.user().onCreate(
//     async (user: admin.auth.UserRecord, context: functions.EventContext) => {
//         const email = user.email;   
//         const link = await admin.auth().generateEmailVerificationLink(email, {
//             url: 'https://my-amazing-app.firebaseapp.com',
//     });
//         // sendEmail() is a helper defined elsewhere that puts the link into a custom
//         // email template, and sends it out using SendGrid.
//         return await sendEmail(email, link);
// });
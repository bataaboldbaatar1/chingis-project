const functions = require('firebase-functions');
const admin = require('firebase-admin');


admin.initializeApp();


exports.deleteEvent = functions.firestore.document("Events/{doc}").onDelete((snapshot) => {
    // console.log(snapshot.data());
    const data = snapshot.data();

    admin.firestore().collection(`users/${data.createdUser}/${data.name}`).onSnapshot((querySnapshot) => {
        setData(querySnapshot.docs.map((doc) => {
            admin.firestore().doc(`users/${data.createdUser}/${data.name}/${doc.id}`).delete()
        }));
    })
});

exports.userComment = functions.firestore.document("Events/{event}/Comments/{doc}").onWrite((change, context) => {

    // console.log(change.after.data());

    admin.firestore().collection(`users/${change.after.data().uid}/Comments/`).doc(`${change.after.data().eventId}`).set({
        comment: change.after.data().comment
    });


});

exports.imageToken = functions.storage.object().onFinalize(async (object) => {
    // await console.log(object);

    let name = object.name;
    let token = object.metadata.firebaseStorageDownloadTokens;
    let selfLink = object.selfLink;
    // console.log(name.split('/')[1], "=======aposugiflkadjgipasfh");

    if (name.split('/')[0] === "Tenders") {
        if (name.split('/')[3] === 'MainImage.jpg') {
            await admin.firestore().collection(`Tenders/`).doc(name.split('/')[2]).set({
                mainImageUrl: `https://firebasestorage.googleapis.com/v0/b/funding-project.appspot.com/o/${selfLink.split('/o/')[1]}?alt=media&token=${token}`
            }, { merge: true })
        } else {
            await admin.firestore().collection(`Tenders/${name.split('/')[2]}/SideImageUrls/`).doc().set({
                url: `https://firebasestorage.googleapis.com/v0/b/funding-project.appspot.com/o/${selfLink.split('/o/')[1]}?alt=media&token=${token}`
            }, { merge: true })
        }
    } else {
        if (name.split('/')[0] === "profileImages") {
            await admin.firestore().collection(`users`).doc(name.split('/')[1]).set({
                profileImageUrl: `https://firebasestorage.googleapis.com/v0/b/funding-project.appspot.com/o/${selfLink.split('/o/')[1]}?alt=media&token=${token}`
            }, { merge: true });
            // console.log(name.split('/')[1])
        }
        if (name.split('/')[2] === 'MainImage.jpg') {
            await admin.firestore().collection(`Events/`).doc(name.split('/')[1]).set({
                mainImageUrl: `https://firebasestorage.googleapis.com/v0/b/funding-project.appspot.com/o/${selfLink.split('/o/')[1]}?alt=media&token=${token}`
            }, { merge: true })
        } else {

            await admin.firestore().collection(`Events/${name.split('/')[1]}/SideImageUrls/`).doc().set({
                url: `https://firebasestorage.googleapis.com/v0/b/funding-project.appspot.com/o/${selfLink.split('/o/')[1]}?alt=media&token=${token}`
            }, { merge: true })
        }
    }
});

// export const sendEmailVerification = functions.auth.user().onCreate(
//     async (user: admin.auth.UserRecord, context: functions.EventContext) => {
//         const email = user.email;   
//         const link = await admin.auth().generateEmailVerificationLink(email, {
//             url: 'https://my-amazing-app.firebaseapp.com',
//     });

//         // sendEmail() is a helper defined elsewhere that puts the link into a custom
//         // email template, and sends it out using SendGrid.
//         return await sendEmail(email, link);
// });
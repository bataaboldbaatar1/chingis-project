import React, {useState, useEffect} from 'react';
import { SignUp } from './pages/sign-up';
import { SignIn } from './pages/sign-in';
import { HomeDefault } from './pages/home-default';
import { AddEvent } from './pages/add-event'
import { Verify } from './pages/verify-account';
import { Verified } from './pages/verified';
import { EventSharing } from './pages/event-sharing';
import { TenderSharing } from './pages/tender-sharing';
import { Components } from './pages/components';
import { Reset } from './pages/reset'
import { Adminz } from './pages/admin-only'
import Profile from './pages/profile'
import { ForgotPass } from './pages/forgot-password'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import { AuthUserProvider } from './providers/auth-user-provider';
import { SignUpProvider } from './providers/sign-up-provider.jsx';
import './style/grid.scss'
import './style/grid-align.scss'
import './style/main.scss';
import { VoteHistory } from './pages/vote-history';
import { Taniltsuulga } from './pages/taniltsuulga';
import { DesktopPage } from './pages/desktop-page';

const App = () => {
    const size = window.innerWidth
    const [width, setWidth] = useState(window.innerWidth)
    useEffect(() => {
        const updating = () => {
            const newWdith = window.innerWidth;
            setWidth(newWdith);
        };

        return window.addEventListener("resize", updating);

    }, [width]);

    if (size >= 1100) {
        return (<DesktopPage />)
    } else {
        return (
            <AuthUserProvider>
                <SignUpProvider>
                    <Router>
                        <Switch>
                            <Route path="/login">
                                <SignIn />
                            </Route>
                            <Route path="/register">
                                <SignUp />
                            </Route>
                            <Route path="/addEvent">
                                <AddEvent />
                            </Route>
                            <Route path="/event-id">
                                <EventSharing />
                            </Route>
                            <Route path="/tender-id">
                                <TenderSharing />
                            </Route>
                            <Route path="/admin-only">
                                <Adminz />
                            </Route>
                            <Route path="/verify-account">
                                <Verify />
                            </Route>
                            <Route path="/verified">
                                <Verified />
                            </Route>
                            <Route path="/profile">
                                <Profile />
                            </Route>
                            <Route path="/vote-history">
                                <VoteHistory />
                            </Route>
                            <Route path="/reset">
                                <Reset />
                            </Route>
                            <Route path="/forgot-pass">
                                <ForgotPass />
                            </Route>
                            <Route path="/feed">
                                <HomeDefault />
                            </Route>
                            <Route path="/components">
                                <Components />
                            </Route>
                            <Route path="/">
                                <Taniltsuulga />
                            </Route>
                        </Switch>
                    </Router>
                </SignUpProvider>
            </AuthUserProvider>
        )
    }
}

export default App

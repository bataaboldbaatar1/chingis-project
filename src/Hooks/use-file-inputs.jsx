import { useState, useRef } from 'react';

export const useFileInput = () => {
    const [files, setFiles] = useState([null, null, null])
    const [images, setImages] = useState(['', '', '']);
    const ref1 = useRef(null);
    const ref2 = useRef(null);
    const ref3 = useRef(null);

    const onSideFileChange1 = () => {
        setFiles((files) => [ref1.current.files[0], files[1], files[2]]);
        setImages((images) => [URL.createObjectURL(ref1.current.files[0]), images[1], images[2]]);
    }

    const onSideFileChange2 = () => {
        setFiles((files) => [files[0], ref2.current.files[0], files[2]]);
        setImages((images) => [images[0], URL.createObjectURL(ref2.current.files[0]), images[2]]);
    }

    const onSideFileChange3 = () => {
        setFiles((files) => [files[0], files[1], ref3.current.files[0]]);
        setImages((images) => [images[0], images[1], URL.createObjectURL(ref3.current.files[0])]);
    }
    
    const removeSideImages1 = () => {
        setImages((images) => ['', images[1], images[2]]);
    }
    const removeSideImages2 = () => {
        setImages((images) => [images[0], '', images[2]]);
    }
    const removeSideImages3 = () => {
        setImages((images) => [images[0], images[1], '']);
    }   

    return { files, images, refs: [ref1, ref2, ref3], onChanges: [onSideFileChange1, onSideFileChange2, onSideFileChange3], removeSide: [removeSideImages1, removeSideImages2, removeSideImages3] }
};
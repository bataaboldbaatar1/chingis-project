import { useEffect } from 'react'

export const useOutsideClick = (ref, callback) => {
    const handleClick = e => {
        if (!ref.current)
            return;
        if (ref.current.contains(e.target))
            return;
        callback();
    };

    useEffect(() => {
        document.addEventListener("click", handleClick, true);
        return () => {
            document.removeEventListener("click", handleClick, true);
        };
    });
};
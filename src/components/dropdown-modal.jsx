import React from 'react';

export const DDModal = ({ pop, closeModal, className, children}) => {
    return (
        <>
            {pop && <div className="backdrop" onClick={closeModal}></div>}
            <div className={`modal flex-center bshadow bradius-5 b-default ${pop && 'modal-animation'}`}>
                {children}
            </div>
        </>
    );
};
import React, { useState } from 'react'
import { useStorage, useCol } from '../Hooks';
import { useHistory } from 'react-router-dom'
import { ProgressBar } from './progress-bar'
import moment from 'moment';
import { Box } from './box';
import { LazyImage } from './lazy-image';

export const EventRender = ({ id, name, desc, vote, voteCount, createdAt }) => {
    const history = useHistory();
    const [more, setMore] = useState(false);
    const url = useStorage(`EventImages/${id}/MainImage.jpg`);
    const cdate = (createdAt && createdAt.toDate()) || Date();
    const { data } = useCol(`Events/${id}/Comments/`);

    return (
        <Box className='flex justify-center mb-15'>
            <div className='flex-center mb-5'>
                <div className="w100">
                    <div className="font-main lh-25 h-24 bold ml-20">
                        {
                            name.length > 30 ?
                                <p className="wbreak fs-14">{name}</p>
                                :
                                <p className="wbreak fs-18">{name}</p>
                        }
                    </div>
                    <div className="font-main fs-14 lh-5 ml-20 c-inactive mt-8 mb-15">{moment(cdate).format('MM сарын DD')}</div>
                    <LazyImage className='flex-center h-250 w90 mt-5 bradius-8' src={url} onClick={() => { history.push(`event-id?id=${id}`) }} />
                    <ProgressBar vote={vote} voteCount={voteCount} />
                    <div className='flex-row justify-between items-center w100 mt-10'>
                        <div className="font-main fs-16 lh-20 c-inactive ml-20">{data.length} Сэтгэгдэл</div>
                        <div className="font-main fs-14 lh-20 mr-20">{vote}/{voteCount} Votes</div>
                    </div>
                    <div className="wbreak font-main fs-14 lh-21 ml-20">
                        {
                            desc.length > 150 ?
                                <p className="wbreak w95">{desc.substring(0, 150)}{!more ? <span className='ul c-primary' onClick={() => { setMore(true) }}><span className="w-20"> </span>SEE MORE</span> : <span onClick={() => { setMore(true) }}>{desc.substring(150, desc.length - 1)}</span>} </p>
                                :
                                <p className="wbreak w95">{desc}</p>
                        }
                    </div>
                </div>
            </div>
        </Box>
    )
}
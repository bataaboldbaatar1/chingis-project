import React from 'react'

export const BlueHomeIcon = ({ height, width, color = "#00000", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 17 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6.8215 20.2858V11.0001H12.1786V20.2858M1.46436 8.21436L9.50007 1.71436L17.5358 8.21436V18.4286C17.5358 18.9212 17.3476 19.3936 17.0128 19.7418C16.6779 20.0901 16.2237 20.2858 15.7501 20.2858H3.25007C2.77647 20.2858 2.32227 20.0901 1.98738 19.7418C1.65249 19.3936 1.46436 18.9212 1.46436 18.4286V8.21436Z" stroke="#2F80ED" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            </svg>

        </span>
    )
}
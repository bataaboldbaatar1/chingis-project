import React from 'react'

export const CalendarIcon = ({ height, width, color = "#999999", ...others }) => {
    return (
        <span {...others} >
            <svg width={width} height={height} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M13.5715 1.07129V4.64272M6.42864 1.07129V4.64272M1.96436 8.21415H18.0358M3.75007 2.857H16.2501C17.2363 2.857 18.0358 3.65649 18.0358 4.64272V17.1427C18.0358 18.1289 17.2363 18.9284 16.2501 18.9284H3.75007C2.76385 18.9284 1.96436 18.1289 1.96436 17.1427V4.64272C1.96436 3.65649 2.76385 2.857 3.75007 2.857Z" stroke="#99A3BE" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
            </svg>
        </span>
    )
}
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _menuIcon = require("./menu-icon");

Object.keys(_menuIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _menuIcon[key];
    }
  });
});

var _xIcon = require("./x-icon");

Object.keys(_xIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _xIcon[key];
    }
  });
});

var _homeIcon = require("./home-icon");

Object.keys(_homeIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _homeIcon[key];
    }
  });
});

var _backIcon = require("./back-icon");

Object.keys(_backIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _backIcon[key];
    }
  });
});

var _profileIcon = require("./profile-icon");

Object.keys(_profileIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _profileIcon[key];
    }
  });
});

var _newProjectIcon = require("./new-project-icon");

Object.keys(_newProjectIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _newProjectIcon[key];
    }
  });
});

var _eyeOff = require("./eye-off");

Object.keys(_eyeOff).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _eyeOff[key];
    }
  });
});

var _searchIcon = require("./search-icon");

Object.keys(_searchIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _searchIcon[key];
    }
  });
});

var _xCircleIcon = require("./x-circle-icon");

Object.keys(_xCircleIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _xCircleIcon[key];
    }
  });
});

var _fbLogo = require("./fb-logo");

Object.keys(_fbLogo).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _fbLogo[key];
    }
  });
});

var _gmailIcon = require("./gmail-icon");

Object.keys(_gmailIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _gmailIcon[key];
    }
  });
});

var _cameraIcon = require("./camera-icon");

Object.keys(_cameraIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _cameraIcon[key];
    }
  });
});

var _downarrow = require("./downarrow");

Object.keys(_downarrow).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _downarrow[key];
    }
  });
});

var _lockIcon = require("./lock-icon");

Object.keys(_lockIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _lockIcon[key];
    }
  });
});

var _mailIcon = require("./mail-icon");

Object.keys(_mailIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _mailIcon[key];
    }
  });
});

var _arrowIcon = require("./arrow-icon");

Object.keys(_arrowIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _arrowIcon[key];
    }
  });
});

var _leftIcon = require("./left-icon");

Object.keys(_leftIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _leftIcon[key];
    }
  });
});

var _rightIcon = require("./right-icon");

Object.keys(_rightIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _rightIcon[key];
    }
  });
});

var _threedotIcon = require("./threedot-icon");

Object.keys(_threedotIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _threedotIcon[key];
    }
  });
});

var _penIcon = require("./pen-icon");

Object.keys(_penIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _penIcon[key];
    }
  });
});

var _trashIcon = require("./trash-icon");

Object.keys(_trashIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _trashIcon[key];
    }
  });
});

var _logOutIcon = require("./log-out-icon");

Object.keys(_logOutIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _logOutIcon[key];
    }
  });
});

var _tanil = require("./tanil1");

Object.keys(_tanil).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _tanil[key];
    }
  });
});

var _eyeOn = require("./eye-on");

Object.keys(_eyeOn).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _eyeOn[key];
    }
  });
});

var _positivity = require("./positivity");

Object.keys(_positivity).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _positivity[key];
    }
  });
});

var _call = require("./call");

Object.keys(_call).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _call[key];
    }
  });
});

var _calendarIcon = require("./calendar-icon");

Object.keys(_calendarIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _calendarIcon[key];
    }
  });
});

var _flameIcon = require("./flame-icon");

Object.keys(_flameIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _flameIcon[key];
    }
  });
});

var _individualIcon = require("./individualIcon");

Object.keys(_individualIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _individualIcon[key];
    }
  });
});

var _groupIcon = require("./groupIcon");

Object.keys(_groupIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _groupIcon[key];
    }
  });
});

var _peopleWithText = require("./peopleWithText");

Object.keys(_peopleWithText).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _peopleWithText[key];
    }
  });
});

var _editIcon = require("./edit-icon");

Object.keys(_editIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _editIcon[key];
    }
  });
});

var _googleIcon = require("./google-icon");

Object.keys(_googleIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _googleIcon[key];
    }
  });
});

var _whiteArrowIcon = require("./white-arrow-icon");

Object.keys(_whiteArrowIcon).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _whiteArrowIcon[key];
    }
  });
});
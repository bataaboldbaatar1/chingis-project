import React from 'react'

export const DownArrow = ({ height, width, color = "#999999", ...others }) => {

    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1 1L7 7L13 1" stroke="#131415" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}
import React from 'react'

export const EditIcon = ({height, width, ...others}) => {

    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="32.5" cy="32.5" r="12.5" fill="#FEFEFE" />
                <path d="M35.3495 24.986L39.3355 28.9719L28.9719 39.3355H24.986V35.3495L35.3495 24.986Z" stroke="#2F80ED" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}
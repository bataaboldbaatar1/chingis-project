import React from 'react'

export const HomeIcon = ({ height, width, color = "#00000", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6.82144 19.2857V9.99995H12.1786V19.2857M1.46429 7.21423L9.50001 0.714233L17.5357 7.21423V17.4285C17.5357 17.9211 17.3476 18.3934 17.0127 18.7417C16.6778 19.09 16.2236 19.2857 15.75 19.2857H3.25001C2.77641 19.2857 2.3222 19.09 1.98732 18.7417C1.65243 18.3934 1.46429 17.9211 1.46429 17.4285V7.21423Z" stroke="#99A3BE" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}
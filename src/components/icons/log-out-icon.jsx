import React from 'react'

export const LogOut = ({ height, width, color = "#00000", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6.82144 17.5357H3.25001C2.77641 17.5357 2.3222 17.3475 1.98732 17.0126C1.65243 16.6778 1.46429 16.2236 1.46429 15.7499V3.24995C1.46429 2.77635 1.65243 2.32214 1.98732 1.98726C2.3222 1.65237 2.77641 1.46423 3.25001 1.46423H6.82144M13.0714 13.9642L17.5357 9.49995M17.5357 9.49995L13.0714 5.03566M17.5357 9.49995H6.82144" stroke="#FB4E4E" strokeLinecap="round" strokeLinejoin="round" />
            </svg>

        </span>
    )
}
import React from 'react'

export const MenuIcon = ({ height, width, color = "#FEFEFE", fill, ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 20 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1 7H19M1 1H12.5M7.5 13H19" stroke="#FEFEFE" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}
export const BlueMenu =  ({ height, width, color = "#2F80ED", fill, ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 20 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1 7H19M1 1H12.5M7.5 13H19" stroke="#FEFEFE" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}
import React from 'react'

export const NewProjectIcon = ({ height, width, color = "#52575C", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M9.49986 5.92855V13.0714M5.92843 9.49998H13.0713M18.4284 9.49998C18.4284 14.4311 14.431 18.4286 9.49986 18.4286C4.56875 18.4286 0.571289 14.4311 0.571289 9.49998C0.571289 4.56887 4.56875 0.571411 9.49986 0.571411C14.431 0.571411 18.4284 4.56887 18.4284 9.49998Z" stroke="#99A3BE" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}
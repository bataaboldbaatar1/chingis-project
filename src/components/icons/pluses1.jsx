import React from 'react'

export const Pluses1 = ({ height, width, ...others }) => {
    return (
        <span {...others}>
            <svg width="112" height="184" viewBox="0 0 112 184" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1 177.8H6M11 177.8H6M6 177.8L6 173L6 183" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M21 177.8H26M31 177.8H26M26 177.8V173V183" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M41 177.8H46M51 177.8H46M46 177.8V173V183" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M1 153.086H6M11 153.086H6M6 153.086L6 148.286L6 158.286" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M21 153.086H26M31 153.086H26M26 153.086V148.286V158.286" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M41 153.086H46M51 153.086H46M46 153.086V148.286V158.286" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M1 128.371H6M11 128.371H6M6 128.371L6 123.571L6 133.571" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M21 128.371H26M31 128.371H26M26 128.371V123.571V133.571" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M41 128.371H46M51 128.371H46M46 128.371V123.571V133.571" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M1 103.657H6M11 103.657H6M6 103.657L6 98.8569L6 108.857" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M21 103.657H26M31 103.657H26M26 103.657V98.8569V108.857" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M41 103.657H46M51 103.657H46M46 103.657V98.8569V108.857" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M1 78.9426H6M11 78.9426H6M6 78.9426L6 74.1426L6 84.1426" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M21 78.9426H26M31 78.9426H26M26 78.9426V74.1426V84.1426" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M41 78.9426H46M51 78.9426H46M46 78.9426V74.1426V84.1426" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M1 54.2287H6M11 54.2287H6M6 54.2287L6 49.4287L6 59.4287" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M21 54.2287H26M31 54.2287H26M26 54.2287V49.4287V59.4287" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M41 54.2287H46M51 54.2287H46M46 54.2287V49.4287V59.4287" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M1 29.5144H6M11 29.5144H6M6 29.5144L6 24.7144L6 34.7144" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M21 29.5144H26M31 29.5144H26M26 29.5144V24.7144V34.7144" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M41 29.5144H46M51 29.5144H46M46 29.5144V24.7144V34.7144" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M1 4.8H6M11 4.8L6 4.8M6 4.8L6 -1.49012e-07L6 10" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M21 4.8H26M31 4.8L26 4.8M26 4.8V-1.49012e-07V10" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M41 4.8H46M51 4.8L46 4.8M46 4.8V-1.49012e-07V10" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M61 177.8H66M71 177.8H66M66 177.8V173V183" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M81 177.8H86M91 177.8H86M86 177.8V173V183" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M101 177.8H106M111 177.8H106M106 177.8V173V183" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M61 153.086H66M71 153.086H66M66 153.086V148.286V158.286" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M81 153.086H86M91 153.086H86M86 153.086V148.286V158.286" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M101 153.086H106M111 153.086H106M106 153.086V148.286V158.286" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M61 128.371H66M71 128.371H66M66 128.371V123.571V133.571" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M81 128.371H86M91 128.371H86M86 128.371V123.571V133.571" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M101 128.371H106M111 128.371H106M106 128.371V123.571V133.571" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M61 103.657H66M71 103.657H66M66 103.657V98.8569V108.857" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M81 103.657H86M91 103.657H86M86 103.657V98.8569V108.857" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M101 103.657H106M111 103.657H106M106 103.657V98.8569V108.857" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M61 78.9426H66M71 78.9426H66M66 78.9426V74.1426V84.1426" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M81 78.9426H86M91 78.9426H86M86 78.9426V74.1426V84.1426" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M101 78.9426H106M111 78.9426H106M106 78.9426V74.1426V84.1426" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M61 54.2287H66M71 54.2287H66M66 54.2287V49.4287V59.4287" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M81 54.2287H86M91 54.2287H86M86 54.2287V49.4287V59.4287" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M101 54.2287H106M111 54.2287H106M106 54.2287V49.4287V59.4287" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M61 29.5144H66M71 29.5144H66M66 29.5144V24.7144V34.7144" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M81 29.5144H86M91 29.5144H86M86 29.5144V24.7144V34.7144" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M101 29.5144H106M111 29.5144H106M106 29.5144V24.7144V34.7144" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M61 4.8H66M71 4.8L66 4.8M66 4.8V-1.49012e-07V10" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M81 4.8H86M91 4.8L86 4.8M86 4.8V-1.49012e-07V10" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M101 4.8H106M111 4.8L106 4.8M106 4.8V-1.49012e-07V10" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
            </svg>
        </span>
    )
}
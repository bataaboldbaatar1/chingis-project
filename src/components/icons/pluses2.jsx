import React from 'react'

export const Pluses2 = ({ height, width, color = "#52575C", ...others }) => {
    return (
        <span {...others}>
            <svg width="184" height="112" viewBox="0 0 184 112" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6.2 1V6M6.2 11V6M6.2 6H11H1" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M6.2 21V26M6.2 31V26M6.2 26H11H1" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M6.2 41V46M6.2 51V46M6.2 46H11H1" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M30.9144 1V6M30.9144 11V6M30.9144 6H35.7144H25.7144" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M30.9144 21V26M30.9144 31V26M30.9144 26H35.7144H25.7144" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M30.9144 41V46M30.9144 51V46M30.9144 46H35.7144H25.7144" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M55.6287 1V6M55.6287 11V6M55.6287 6H60.4287H50.4287" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M55.6287 21V26M55.6287 31V26M55.6287 26H60.4287H50.4287" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M55.6287 41V46M55.6287 51V46M55.6287 46H60.4287H50.4287" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M80.3431 1V6M80.3431 11V6M80.3431 6H85.1431H75.1431" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M80.3431 21V26M80.3431 31V26M80.3431 26H85.1431H75.1431" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M80.3431 41V46M80.3431 51V46M80.3431 46H85.1431H75.1431" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M105.057 1V6M105.057 11V6M105.057 6H109.857H99.8574" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M105.057 21V26M105.057 31V26M105.057 26H109.857H99.8574" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M105.057 41V46M105.057 51V46M105.057 46H109.857H99.8574" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M129.771 1V6M129.771 11V6M129.771 6H134.571H124.571" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M129.771 21V26M129.771 31V26M129.771 26H134.571H124.571" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M129.771 41V46M129.771 51V46M129.771 46H134.571H124.571" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M154.486 1V6M154.486 11V6M154.486 6H159.286H149.286" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M154.486 21V26M154.486 31V26M154.486 26H159.286H149.286" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M154.486 41V46M154.486 51V46M154.486 46H159.286H149.286" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M179.2 1V6M179.2 11V6M179.2 6H184H174" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M179.2 21V26M179.2 31V26M179.2 26H184H174" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M179.2 41V46M179.2 51V46M179.2 46H184H174" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M6.2 61V66M6.2 71V66M6.2 66H11H1" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M6.2 81V86M6.2 91V86M6.2 86H11H1" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M6.2 101V106M6.2 111V106M6.2 106H11H1" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M30.9144 61V66M30.9144 71V66M30.9144 66H35.7144H25.7144" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M30.9144 81V86M30.9144 91V86M30.9144 86H35.7144H25.7144" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M30.9144 101V106M30.9144 111V106M30.9144 106H35.7144H25.7144" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M55.6287 61V66M55.6287 71V66M55.6287 66H60.4287H50.4287" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M55.6287 81V86M55.6287 91V86M55.6287 86H60.4287H50.4287" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M55.6287 101V106M55.6287 111V106M55.6287 106H60.4287H50.4287" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M80.3431 61V66M80.3431 71V66M80.3431 66H85.1431H75.1431" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M80.3431 81V86M80.3431 91V86M80.3431 86H85.1431H75.1431" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M80.3431 101V106M80.3431 111V106M80.3431 106H85.1431H75.1431" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M105.057 61V66M105.057 71V66M105.057 66H109.857H99.8574" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M105.057 81V86M105.057 91V86M105.057 86H109.857H99.8574" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M105.057 101V106M105.057 111V106M105.057 106H109.857H99.8574" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M129.771 61V66M129.771 71V66M129.771 66H134.571H124.571" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M129.771 81V86M129.771 91V86M129.771 86H134.571H124.571" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M129.771 101V106M129.771 111V106M129.771 106H134.571H124.571" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M154.486 61V66M154.486 71V66M154.486 66H159.286H149.286" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M154.486 81V86M154.486 91V86M154.486 86H159.286H149.286" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M154.486 101V106M154.486 111V106M154.486 106H159.286H149.286" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M179.2 61V66M179.2 71V66M179.2 66H184H174" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M179.2 81V86M179.2 91V86M179.2 86H184H174" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
                <path d="M179.2 101V106M179.2 111V106M179.2 106H184H174" stroke="#2F80ED" stroke-opacity="0.2" stroke-linecap="round" />
            </svg>
        </span>
    )
}
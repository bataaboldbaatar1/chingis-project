import React from 'react'

export const Tanil1 = ({ height, width, color = "#52575C", ...others }) => {
    return (
        <span {...others}>
            <svg width="191" height="126" viewBox="0 0 191 126" fill="none" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
                <path d="M185.661 51.8132C185.661 101.519 145.367 124.813 95.6611 124.813C45.9555 124.813 5.66113 101.519 5.66113 51.8132" stroke="black" />
                <rect x="148.062" y="8.81323" width="4.8" height="15" rx="2.4" fill="#FFCD69" />
                <rect x="142.661" y="18.4132" width="4.8" height="15" rx="2.4" transform="rotate(-90 142.661 18.4132)" fill="#FFCD69" />
                <rect x="43.0615" y="65.8132" width="4.8" height="15" rx="2.4" fill="black" />
                <rect x="37.6611" y="75.4132" width="4.8" height="15" rx="2.4" transform="rotate(-90 37.6611 75.4132)" fill="black" />
                <rect x="184.261" y="36.8132" width="3.2" height="10" rx="1.6" fill="#FFCD69" />
                <rect x="180.661" y="43.2133" width="3.2" height="10" rx="1.6" transform="rotate(-90 180.661 43.2133)" fill="#FFCD69" />
                <rect x="146.062" y="39.8132" width="4.8" height="15" rx="2.4" fill="#FF9171" />
                <rect x="140.661" y="49.4132" width="4.8" height="15" rx="2.4" transform="rotate(-90 140.661 49.4132)" fill="#FF9171" />
                <rect x="82.8613" y="76.8132" width="6.4" height="20" rx="3.2" fill="#E2E9EE" />
                <rect x="75.6611" y="89.6133" width="6.4" height="20" rx="3.2" transform="rotate(-90 75.6611 89.6133)" fill="#E2E9EE" />
                <circle cx="128.161" cy="96.3132" r="6" stroke="#D5EA79" strokeWidth="3" />
                <circle cx="47.1611" cy="36.3132" r="6" stroke="#FFE6B4" strokeWidth="3" />
                <circle cx="120.661" cy="60.8132" r="3.5" stroke="#FFCD69" strokeWidth="3" />
                <circle cx="5.66113" cy="41.8132" r="4" stroke="#1DD3D2" strokeWidth="2" />
                <rect x="65.6611" y="0.813232" width="60" height="60" fill="url(#pattern0)" />
                <defs>
                    <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                        <use href="#image0" transform="scale(0.00195312)"/>
                    </pattern>
                    <div className='img1' ></div>
                </defs>
            </svg>
        </span >
    )
}

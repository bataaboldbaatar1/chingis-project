import React from 'react'

export const WhiteArrowIcon = ({ height, width, color = "#999999", ...others }) => {
    return (
        <span {...others} >
            <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M18.75 12.5H6.25M6.25 12.5L12.5 18.75M6.25 12.5L12.5 6.25" stroke="#FEFEFE" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}
import React, { useState } from 'react';
import { useEffect } from 'react';
import { Loader } from './loader';

export const LazyImage = ({ src, className, ...others }) => {
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        if (src) {
            const imageLoader = new Image();
            imageLoader.src = src;
            imageLoader.onload = () => setLoading(false)
        }
    }, [src])

    return (
        <div className={`pr w100 h-250 ${className}`}
            style={{ backgroundSize: 'cover', backgroundImage: `url("${loading ? '' : src}")` }}
            {...others}
        >
            {loading &&
                <div className={`absolute flex-center`} style={{ top: 0, left: 0, right: 0, bottom: 0 }}>
                    <Loader width={50} height={50} />
                </div>}
        </div>
    )
}
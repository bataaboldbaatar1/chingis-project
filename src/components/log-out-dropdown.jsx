import React from 'react';
import { useHistory } from 'react-router-dom'
import { useFirebase } from '../Hooks';

export const LogOutDropdown = () => {
    const history = useHistory();
    const { auth } = useFirebase();

    return (
        <div className="flex flex-center">
            <div className="h-44 flex flex-center lh-20 fs-16 font-main pa-5 b-default t-45 w-vw-90 mb-1 brad-top-10 text-center c-gray">Та гарахдаа итгэлтэй байна уу?</div>
            <div className="h-44 flex flex-center lh-20 fs-16 font-main pa-5 b-default t-45 w-vw-90 mt-1 brad-bottom-10 text-center c-warning-second" onClick={() => { auth.signOut(); history.push('/'); console.log("signed out") }}>Гарах</div>
            <div className="h-44 flex flex-center lh-20 fs-16 font-main pa-5 b-default t-45 w-vw-90 mt-5 brad-bottom-10 brad-top-10 text-center c-fb-color" onClick={() => { history.push('/'); console.log("home") }}>Цуцлах</div>
        </div>
    )
}
import React from 'react';

export const ModalSlider = ({ show, closeSlider, className, children}) => {
    return (
        <>
            {show && <div className="backdrop" onClick={closeSlider}></div>}
            <div className={`slider flex-center bshadow bradius-5 ${show && 'slider-animation'}`}>
                {children}
            </div>
        </>
    );
};
import React, { useState } from 'react';
import { DotIcon, ModalSlider, Box } from '../components';
import { useHistory } from 'react-router-dom'
import { ProgressBar } from './progress-bar';

export const RenderProfilePost = ({ vote, voteCount, name, desc, mainImageUrl, id, deleteEvent }) => {

    const [show, setShow] = useState(false);
    const [more, setMore] = useState(false);
    const history = useHistory();

    const handleShow = () => {

        setShow(!show)
    }

    return (
        <Box className="flex justify-center pv-20">
        <div className='pr'>
            <div className='flex justify-between items-center'>
                <div className='font-main fs-24 fw700'>{name}</div>
                <DotIcon className="pa-5" width={25} height={25} onClick={handleShow} />
            </div>
            <div onClick={() => { history.push(`event-id?id=${id}`) }} style={{ backgroundColor: 'lightgray', backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundImage: `url("${mainImageUrl}")` }} className='bradius-10 w100 h30' ></div>
           
            <ProgressBar />
            {
                desc.length > 150 ?
                    <p className="wbreak font-main">{desc.substring(0, 150)}{!more ? <span className='ul c-primary' onClick={() => { setMore(true) }}> ... see more</span> : <span onClick={() => { setMore(true) }}>{desc.substring(150, desc.length - 1)}</span>} </p>
                    :
                    <p className="wbreak font-main">{desc}</p>
            }


            
            <ModalSlider show={show} closeSlider={(show) => {setShow(!show)}}>
                <div className="flex flex-center">
                    <div className="h-44 lh-20 fs-16 font-main pa-5 b-default t-45 w-vw-90 mb-1 brad-top-10 text-center c-gray">Та энэ аянг усгахдаа итгэлтэй байна уу ?</div>
                    <div onClick={() => {deleteEvent(id)}} className="h-44 lh-20 fs-16 font-main pa-5 b-default t-45 w-vw-90 mt-1 brad-bottom-10 text-center c-warning-second">Аянг устгах</div>
                    <div className="h-44 lh-20 fs-16 font-main pa-5 b-default t-45 w-vw-90 mt-5 brad-bottom-10 brad-top-10 text-center c-fb-color">Цуцлах</div>
                </div>
            </ModalSlider>
        </div>
        </Box>
    )
}
import React, { useState } from "react";
import { SearchIcon, XCircleIcon } from './index';
import { Input } from "./input";
import { useCol } from "../Hooks/firebase";

export const SearchInput = (props) => {
    let { className, updateSearch, type, addTag, placeholder, ...others } = props;
    const [focus, setFocus] = useState(false);
    const [search, setSearch] = useState('');

    const clearSearch = () => {
        updateSearch && updateSearch('');
        setSearch('')
    }

    const clear1 = () => {
        setSearch('');
    }
    const changeSearch = (e) => {
        updateSearch && updateSearch(e.target.value);
        setSearch(e.target.value)
    }
    const selectCategory = (category) => {
        updateSearch && updateSearch(category);
        addTag && addTag(category);
        setSearch(category);
    }

    const { data } = useCol('Categories');
    const filteredData = data.filter((item) => item.category && item.category.toLowerCase().includes(search.toLowerCase()));


    const onFocus = () => {
        setFocus(true)
    }

    const onBlur = () => {
        setTimeout(() => setFocus(false), 20)
    }

    const itemClick = (item) => () => {
        selectCategory(item.category)
        clear1()
    }

    return (
        <>

            {
                (type !== "addEvent")
                    ?
                    <div className='searchBar w90'>
                        <Input value={search} onChange={changeSearch} onBlur={onBlur} onFocus={onFocus} className={`searchInput searchtext font-main b-gray6 w100 h-35 fs-16 lh-20 v-align ${className} mt-1`} type="text" {...others} placeholder={`${placeholder}`} />
                        <SearchIcon className='searchIcon mt-3 ml-5' width={19} height={19} />  
                        <div className='dropDown flex-center b-default text-align of w100 absolute z-3 brad-top-10 brad-bottom-10'>
                            {(filteredData && focus) && filteredData.map((item) =>
                                <div key={item.id} className='flex items-center z-2 relative b-default w95 bb-blue-1 c-black searchtext fw-400 font-main h-35 fs-16 lh-20'>
                                    <div onClick={() => selectCategory(item.category)} className='ml-10'>
                                        {item.category}
                                    </div>
                                </div>
                            )}
                        </div>


                        {search !== '' && <XCircleIcon onClick={clearSearch} className='closeIcon ' width={25} height={25} />}
                    </div>
                    :
                    //This is SearchBar components.
                    <div className='searchBar w90'>
                        <Input value={search} onChange={changeSearch} onBlur={onBlur} onFocus={onFocus} className={`searchInput searchtext font-main b-gray6 w100 h-35 fs-16 lh-20 v-align ${className} mt-1`} type="text" {...others} placeholder="Ангилал хайх" />
                        <SearchIcon className='searchIcon mt-3 ml-5' width={19} height={19} />
                        <div className='flex-center dropDown b-white w100 absolute of-y z-3 b-white'>
                            {(filteredData && focus) && filteredData.map((item) =>
                                <div key={item.id} className='flex items-center b-white z-2 w95 b-default bb-dark-1 searchtext fw-400 font-main h-35 fs-16 lh-20'>
                                    <div onClick={itemClick(item)}>
                                        {item.category}
                                    </div>
                                </div>
                            )}
                        </div>

                        {search !== '' && <XCircleIcon onClick={clearSearch} className='closeIcon ' width={25} height={25} />}
                    </div>
            }
        </>
    );
};

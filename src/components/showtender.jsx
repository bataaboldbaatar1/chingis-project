import React, { useState } from 'react';
// import {Stack} from '../components';
import { TenderRender } from './tender-render';
import { SortBy } from './sortby';
// import { Box } from './box';

export const ShowTender = ({ data }) => {

    const [sort, setSort] = useState('Шинэ');

    return (
        <div className="container-bruh">
            <div className="flex-row justify-between items-center w100">
                <div className="font-main fs-24 lh-31 bold mt-10 ml-20 c-fb-color">Тендер</div>
                <SortBy type={sort} setType={setSort} className="mt-2" />
            </div>
            <div className='flex-col mt-16 mb-16'>
                {sort === 'Шинэ' && data.sort((a, b) => a.vote > b.vote ? -1 : 1).map((e) => <TenderRender key={e.id} {...e} />)}
                {sort === 'Хандалт ихтэй' && data && data.map((e) => <TenderRender key={e.id} {...e} />)}
            </div>
        </div>
    )
}
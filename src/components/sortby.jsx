import React from 'react';
import { DropdownMenuArrow } from './dropdown-menu';

export const SortBy = ({ type, setType }) => {

    // console.log(type);
    return (
        <div>
            <DropdownMenuArrow type={type} setType={setType}/>
        </div>
    )
}
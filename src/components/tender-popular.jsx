import React, { useState } from 'react';

export const TenderCampaign = ({ toggle }) => {
    const [clicked, setClicked] = useState(false)

    const toggleAnimation = () => {
        if (clicked === false) {
            setClicked(true)
            toggle()
        } else {
            setClicked(false)
            toggle()
        }
    }

    return (
        <div className="flex-col mt-20 h-36 bradius-10">
            <div className="toggle-menu pr w90 flex-row h-36">
                <div className={`flex-center w50 font-main text-center mt-5 bradius-top-left bradius-top-right ${clicked ? 'c-white bold' : 'c-white bold bb-fb-color-2 white-gradient'}`} onClick={toggleAnimation}>Тендер</div>
                <div className={`flex-center w50 font-main text-center mt-5 bradius-top-right bradius-top-left ${clicked ? 'c-white bold bb-fb-color-2 white-gradient' : 'c-white bold'}`} onClick={toggleAnimation}>Аян</div>
                <div className={`b-secondary-gradient h-36 menu-indicator op-little bradius-top-left bradius-top-right ${clicked ? 'active' : ''}`} />
            </div>
        </div>
    )
}
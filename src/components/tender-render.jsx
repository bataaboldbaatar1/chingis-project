import React, { useState } from 'react'
import { useStorage } from '../Hooks';
import { useCol } from '../Hooks'
import { useHistory } from 'react-router-dom'
import { Box } from './box'
import moment from 'moment';
import { LazyImage } from './lazy-image';

export const TenderRender = ({ id, name, desc, createdAt, funds }) => {
    const history = useHistory();
    const url = useStorage(`Tenders/Events/${id}/MainImage.jpg`);
    const { data } = useCol(`Tenders/${id}/Comments/`);
    const [more, setMore] = useState(false);
    const cdate = (createdAt && createdAt.toDate()) || Date();

    return (
        <Box className="flex justify-center mb-15">
            <div className='flex-center'>
                <div className="w100">
                    <div className="font-main fs-18 lh-25 h-24 bold ml-20">
                        {
                            name.length > 30 ?
                                <p className="wbreak fs-14">{name}</p>
                                :
                                <p className="wbreak fs-18">{name}</p>
                        }
                    </div>
                    <div className="font-main fs-14 lh-5 ml-20 c-inactive mt-8 mb-15">{moment(cdate).format('MM сарын DD')}</div>
                    <LazyImage className='h-250 w90 mt-5 bradius-8' src={url} onClick={() => { history.push(`tender-id?id=${id}`) }} />
                    <div className='flex-row justify-between items-center w90 mt-10'>
                        <div className="font-main fs-16 lh-24 c-inactive">{data.length} Сэтгэгдэл</div>
                        <div className="font-main fs-16 lh-24 c-black">Дүн {funds}₮</div>
                    </div>
                    <div className="font-main fs-14 lh-21 ml-20 mr-20">
                        {
                            desc.length > 150 ?
                                <p className="wbreak w95">{desc.substring(0, 150)}{!more ? <span className='ul c-primary' onClick={() => { setMore(true) }}>SEE MORE</span> : <span onClick={() => { setMore(true) }}>{desc.substring(150, desc.length - 1)}</span>} </p>
                                :
                                <p className="wbreak w95">{desc}</p>
                        }
                    </div>
                </div>
            </div>
        </Box>
    )
}

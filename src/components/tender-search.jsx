import React, { useEffect, useState, useRef } from "react";
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { Input } from "./input";
import { useCol } from "../Hooks/firebase";


export const TenderSearch = () => {
    const [search, setSearch] = useState('');
    console.log(search)
    const ref = useRef();

    const { data } = useCol('Tenders');
    const filteredData = data.filter((item) => item.tender && item.tender.includes(search));

    useEffect(() => {
        if (ref.current) {
            const keyup$ = fromEvent(ref.current, 'keyup')

            const subscribe = keyup$.pipe(
                map((e) => e.currentTarget.value),
                debounceTime(500)
            ).subscribe(setSearch)

            return () => subscribe.unsubscribe();
        }
    }, [ref])

    return (
        <div>
            <Input inputRef={ref} type="text" placeholder="Search..." />
            {filteredData && filteredData.map((item) => <li>{item.category}</li>)}
        </div>
    )
}
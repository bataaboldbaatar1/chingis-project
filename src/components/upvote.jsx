import React from 'react';
import { useDoc, useCol } from '../Hooks/firebase';
import { useState } from 'react';
// import { useEffect } from 'react';
import { FlameIcon } from '../components'


export const Upvote = (props) => {
    let { children, disabled, className, upvoted, eventId, date, uid, projectName, ...others } = props;
    let { data: username } = useDoc(`users/${uid}`);
    let { createRecord } = useCol(`Events/${eventId}/votes`);
    let { updateRecord: eventInfo } = useDoc(`Events/${eventId}`);

    let { deleteRecord, data: pr } = useCol(`Events/${eventId}/votes/`);
    let { deleteRecord: deleteRecord2 } = useCol(`users/${uid}/votes`);
    const { loading: voteLoading } = useDoc(`users/${uid}/votes/${eventId}`);
    let { createRecord: createRecord2 } = useCol(`users/${uid}/votes`);


    const isLoaded = (...loaders) => loaders.reduce((condition, loader) => condition || loader, false);
    
    const [state, setState] = useState(false);
    // let a = voter && voter.projectId === eventId;
    // useEffect(() => {
    //     setState(a);
    // }, [a])

    let upvote = () => {
        console.log('Irswn')
        if (state === false) {
            console.log(pr.length, " <==length")
            // console.log(username)
            createRecord(username && username.id, { 
                createdAt: date, 
                userId: uid, 
                // username: username.username, 
                projectId: eventId });
            createRecord2(eventId, { createdAt: date, projectId: eventId, projectName: projectName });
            eventInfo({'vote': pr.length + 1})
            setState(true);
        } else {
            console.log(pr.length, " <==length")
            deleteRecord(username && username.id);
            deleteRecord2(eventId)
            eventInfo({'vote': pr.length - 1})
            setState(false);
        }
    }

    if (isLoaded(voteLoading)) {
        return (
            <div className='flex-center h-vh-80 bold'>
                <h3>Loading ...</h3>
                <div className="loader"></div>
            </div>
        )
    }

    return (
        <>
            {
                state ? 
                    <button className='margin-auto w100 b-white br-blue-2 bradius-5 flex-row primary-gra2 justify-center items-center outl' {...others} onClick={() => {setState(false); upvote()}}>
                        <p className="c-white bold fs-16 fw700 mr-8 " >Дэмжсэн</p>
                        <FlameIcon width={13} height={16} color={'#FFFFFF'} bColor={'#FFFFFF'} />
                    </button>
                :
                    <button className='margin-auto w100 b-white br-blue-2 bradius-5 flex-row justify-center items-center outl' {...others} onClick={() => {setState(true); upvote()}}>
                        <p className="c-gra bold fs-16 fw700 mr-8" >Дэмжих</p>
                        <FlameIcon width={13} height={16} />
                    </button>
            }
        </>
    );
};
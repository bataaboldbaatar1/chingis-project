import React, { useState, useContext, useRef, useEffect } from 'react';
import { Box, FormInput } from '../components';
import { useCol, useDoc } from '../Hooks/firebase';
import { AuthContext } from '../providers/auth-user-provider'
import { useFirebase } from '../Hooks/firebase';
import { useHistory } from 'react-router-dom'
import { SearchInput } from '../components/search-input'
import { CameraIcon } from '../components/icons/camera-icon'
import { ArrowIcon } from '../components/icons/arrow-icon'
import '../style/main.scss'
import { XIconBack } from '../components/icons/x-with-back-icon';
// import { PenIcon } from '../components/icons/pen-icon'
import { useFileInput } from '../Hooks/use-file-inputs';
import { Loader } from '../components/loader'

// import { map } from 'lodash';

// AddEvent.map((el) => {
// })
// console.log(location, eventName, desc, date, voteCount, funds, search, imageSrc, sideImages)

export const AddEvent = () => {
    let checkTender = new URLSearchParams(window.location.search).get('tender');
    const { user } = useContext(AuthContext)
    const history = useHistory();
    const [location, setLocation] = useState('');
    const [eventName, setEventName] = useState('');
    const [desc, setDesc] = useState('');
    const [file, setFile] = useState('');
    const [date, setDate] = useState('');
    const [voteCount, setVoteCount] = useState('');
    const [imageSrc, setImageSrc] = useState('');
    const [funds, setFunds] = useState('');
    const [search, setSearch] = useState('');
    const [loader, setLoader] = useState(false);
    const { firebase } = useFirebase();
    const inputFile = useRef(null);
    // const [sideImages, setSideImages] = useState('')
    const { images: sideImages, files: sideFiles, refs, onChanges, removeSide } = useFileInput();
    console.log(date)
    // console.log('file:',file,)
    // console.log('imageSrc', imageSrc)
    // console.log('sideimages', sideImages)
    // console.log('sideFiles', sideFiles)

    const isLoaded = (...loaders) => loaders.reduce((condition, loader) => condition || loader, false);

    let uid;

    if (user != null) {
        uid = user.uid
    }


    const { createRecord } = useCol(`/Events/`);
    const { createRecord: createTenderEvent } = useCol(`Tenders/${checkTender}/Events`)
    const { createRecord: createTender } = useCol(`/Tenders/`);
    const { data: tenderInfo } = useDoc(`/Tenders/${checkTender && checkTender}`);
    const { data: logged, loading: userLoading, } = useDoc(`/users/${uid}`);
    const { createRecord: createCreateTender } = useCol(`/users/${uid}/createdTenders`);
    const { createRecord: createCreateEvent } = useCol(`/users/${uid}/createdEvents`);
    const [cate, setCate] = useState([])
    const { data } = useCol(`/Categories`);
    const [tags, setTags] = useState({});

    const handleChangeLocation = (e) => setLocation(e.target.value);
    const handleChangeEventName = (e) => setEventName(e.target.value);
    const handleChangeDesc = (e) => setDesc(e.target.value);
    const handleChangeVoteCount = (e) => setVoteCount(e.target.value);
    const handleChangeFunds = (e) => setFunds(e.target.value);
    const handleChangeDate = (e) => setDate(e.target.value);
    console.log(Object.values(tags).length)

    const randomStringAndNumber = () => {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < 7; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    const removeImage = () => {
        setImageSrc('')
    }

    useEffect(() => {
        if (search != '') {
            // let searches = search.split(' ');
            // console.log(search)

            // search.map((e) => {
            // console.log(data)
            // console.log(search)
            // setCate(
            // ...cate,
            data.map((g) => {
                if (search === g.category) {
                    console.log(search, " -==- ", g.category)
                    setCate([...cate, search])
                    return search;
                }
            })

            console.log(cate);
            // )
            // console.log(cate);
            // })
        }
    }, [search])

    const Add = async () => {
        setLoader(true)
        if (
            location === "" ||
            eventName === "" ||
            desc === "" ||
            date === "" ||
            // voteCount === "" ||
            imageSrc === "" ||
            sideImages === ["", "", ""]
            // Object.values(tags).length === 0
            // sideFiles === [null, null, null] ||
            // file.length === 0
        ) {
            alert('ta buren buglunu uu')
            setLoader(false)
            return
        }
        console.log('shalgalt davsan')
        let id = randomStringAndNumber();

        console.log(cate);

        if (logged.logged !== "Group") {
            if (file) {
                var storageRef2 = firebase.storage().ref().child(`EventImages/${id}/MainImage.jpg`);;
                await storageRef2.put(file)
                    .then((snapshot) => {
                        console.log('Done.');
                    })
                await sideFiles.map((e, i) => {
                    // console.log(e);
                    var storageSideRef = firebase.storage().ref().child(`EventImages/${id}/SideImage-${i}.jpg`);;
                    storageSideRef.put(e)
                        .then((snapshot) => {
                            console.log('Done. SideImage');
                        })
                    return storageSideRef;
                })
            }

            await createRecord(id, { name: eventName, desc: desc, location: location, id: id, categories: cate, tender: checkTender && checkTender, createdUser: user.uid, vote: 0, voteCount: Number(voteCount), createdAt: firebase.firestore.FieldValue.serverTimestamp(), endsAt: date });

            await createCreateEvent(id, { id: id })
            checkTender && checkTender ? await createTenderEvent(id, { id: id }) : console.log('not tender');
        } else {
            let tid = randomStringAndNumber();
            if (file) {
                console.log('aaad')
                var storageRef = firebase.storage().ref().child(`Tenders/Events/${tid}/MainImage.jpg`);;
                await storageRef.put(file)
                    .then((snapshot) => {
                        console.log('Done.');
                    })
                await sideFiles.map((e, i) => {
                    console.log(e);
                    var storageSideRef = firebase.storage().ref().child(`Tenders/Events/${tid}/SideImage-${i}.jpg`);;
                    storageSideRef.put(e)
                        .then((snapshot) => {
                            console.log('Done. SideImage');
                        })
                    return storageSideRef;
                })
            }

            await createTender(tid, {
                name: eventName, desc: desc, location: location, tenderId: tid, categories: cate,
                createdUser: user.uid, vote: 0, voteCount: Number(voteCount), funds: funds, createdAt: firebase.firestore.FieldValue.serverTimestamp()
            }); 
            await createCreateTender(id, { id: id })
        }

        setLoader(false)
        history.push('/')
    }

    const remove = (tag) => {
        const filteredTags = Object.keys(tags).filter((obj) => obj !== tag);
        if (filteredTags.length === 0)
            setTags({});
        setTags(filteredTags.reduce((res, field) => ({ ...res, [field]: field }), {}))
    }

    const onFileChange = () => {
        setFile(inputFile.current.files[0]);
        setImageSrc(URL.createObjectURL(inputFile.current.files[0]))
    }
    if (isLoaded(userLoading)) {
        return (
            <div className='flex-center w100 h-vh-90'>
                <Loader height={125} width={125} />
            </div>
        )
    }

    // const error = () => {
    if (loader) {
        return (
            <div className='flex-center w100 h-vh-90'>
                <Loader height={125} width={125} />
            </div>
        )
    }
    // }

    // console.log(tags)
    return (
        <div className="font-main w-vw-100 b-blue-dark margin-auto">
            <Box className="" >
                <div className="brad-bottom-16 w-vw-100 p-f z-i b-white h-vh-10 top-0 boxshadow-2 flex margin-auto justify-end">
                    <ArrowIcon className="ml-20 flex justify-start items-center ws30" onClick={() => { history.goBack() }} width={20} height={20} />
                    <div className="font-main bold ib flex flex-center fs-21 ws33 text-center">Аян Нэмэх</div>
                    {/* example of using button component */}
                    {
                        (logged && logged.logged === "Group")
                            ?
                            <div className="font-main ws33 flex flex-center text-end bold fs-18 c-blue" onClick={Add} style={{ cursor: 'pointer' }} >Тендер зарлах</div>
                            :
                            <div className="font-main ws33 flex flex-center text-end bold fs-18 c-blue" onClick={Add} style={{ cursor: 'pointer' }} >{checkTender ? "Оролцох" : "Нийтлэх"}</div>
                    }
                </div>

                {/* </form> */}
                <div id='form' className="m-vh-top-10 ">
                    {/* <div className="h-1 w-vw-100 b-gray4 " ></div> */}
                    <FormInput typeSecond="event" className="input rb font-main fs-18 lh-18" placeholder='Гарчиг нэмэх' value={eventName} onChange={handleChangeEventName} />
                    <div className="h-1 w-vw-100 b-gray4 " ></div>
                    <FormInput typeSecond="event" className="input rb font-main fs-18 h-vh-20" placeholder='Тайлбар үүсгэх' value={desc} onChange={handleChangeDesc} />
                    <div className="h-1 w-vw-100 b-gray4 " ></div>
                    <FormInput typeSecond="event" className="input rb font-main fs-18" placeholder='Байршил нэмэх' value={location} onChange={handleChangeLocation} />
                </div>
            </Box>

            {/* <div className="h-1 w-vw-100  fs-18 lh-23" ></div> */}
            {/* {
                (logged && logged.logged === "Group") ?
                    <div>
                        <FormInput className="input bradius-10 b-blue-dark rb ph-12 pa-8 fs-16 font-main onlyAddEvent" type="number" placeholder='Хандивын дүн' value={funds} onChange={handleChangeFunds} />
                    </div>
                    :
                    <div></div>
            } */}
            <Box className="flex justify-center pa-t-25 mt-10">
                {/* add category  */}
                <div id="category" className="w-vw-90 margin-auto">
                    <div className="flex flex-row justify-between fs-18 lh-23 mb-15 font-main">
                        <div className="bold">Ангилал</div>
                        <div>{Object.values(tags).length}/3</div>
                    </div>
                    <div className="w-vw-100 ml-stupid">
                        <SearchInput type='addEvent' className="mb-10 onlyAddEvent" updateSearch={setSearch} addTag={(tag) => {
                            if (Object.values(tags).length <= 2) setTags((tags) => ({ ...tags, [tag]: tag }))
                        }} />
                    </div>

                    {
                        tags && Object.values(tags).map((tag, index) =>
                            <div className="ib ms" key={index}>
                                <div className="pa-10 bradius-10 w-fit flex-row br-primary-1 items-center">
                                    <div className=" ">{tag}</div>
                                    <XIconBack className=' closeIcon ml-10' onClick={() => remove(tag)} width={25} height={25} />
                                </div>
                            </div>
                        )
                    }


                </div>

                <div className="h-1 w-vw-100 b-gray4 mline fs-18 lh-23" ></div>

                {/* number of vote */}
                <div id="number of vote" className="w-vw-90 margin-auto">
                    {
                        (logged && logged.logged === "Group")
                            ?
                            <div className="font-main mb-24 fs-18 bold">Хандивын дүн</div>
                            :
                            <div className="bold font-main mb-24 fs-18">Саналын дүн</div>
                    }
                    {
                        (logged && logged.logged === "Group")
                            ?
                            <FormInput className="input bradius-10 b-blue-dark rb ph-12 pa-8 fs-16 font-main onlyAddEvent" type="number" placeholder='Мөнгөн дүн' value={funds} onChange={handleChangeFunds} />
                            :
                            <FormInput type="number" className="input bradius-10 b-blue-dark rb ph-12 pa-8 fs-16 font-main onlyAddEvent" placeholder="Саналын дүн" value={voteCount} onChange={handleChangeVoteCount}></FormInput>
                            // <FormInput type="number" className="input bradius-10 b-blue-dark rb ph-12 pa-12 fs-16 font-main" placeholder="number of vote" value={voteCount} onChange={handleChangeVoteCount}></FormInput>
                    }
                </div>

                <div className="h-1 w-vw-100 mline b-gray4 fs-18 lh-23 " ></div>

                {/* date */}
                <div id="date" className="w-vw-90 margin-auto">
                    <div className="bold font-main fs-18 mb-24">Дуусах</div>
                    <FormInput type="date" value={date} onChange={handleChangeDate} className="onlyAddEvent w-vw-90 rb b-blue-dark font-main bradius-10 pa-8" placeholder="mm/dd/yyyy"></FormInput>
                </div>
                {/* <div className="h-1 w-vw-100 mline fs-18 lh-23" ></div> */}
            </Box>

            <Box className="flex justify-center pa-t-25 mt-10">
                <div id='take-photo' className="w-vw-90 margin-auto">
                    {/* zurag avna */}
                    <div className="bold font-main fs-18 mb-23">Зураг нэмэх</div>

                    <input onChange={() => onFileChange(0)} type='file' id='file' ref={inputFile} style={{ display: 'none' }} />

                    <div className="h-vh-35 w100 flex-col items-center brad-8 bs-contain br-no bp-center b-blue-dark " style={{ backgroundImage: `url("${imageSrc}")` }}>
                        {
                            (imageSrc === '')
                                ?
                                <div className="w96 hs35 pa-vw-2 flex justify-end"></div>
                                :
                                <div className="w96 hs35 pa-vw-2 flex justify-end">
                                    <XIconBack height={30} width={30} onClick={removeImage} />
                                </div>
                        }
                        <CameraIcon height={30} width={30} onClick={() => { inputFile.current.click() }} />
                    </div>
                    {/* add multiple file */}
                    <div id='take-side-images'>
                        <input onChange={onChanges[0]} type='file' id='sidefile' ref={refs[0]} style={{ display: 'none' }} />
                        <input onChange={onChanges[1]} type='file' id='sidefile' ref={refs[1]} style={{ display: 'none' }} />
                        <input onChange={onChanges[2]} type='file' id='sidefile' ref={refs[2]} style={{ display: 'none' }} />
                        <div className="flex flex-row justify-between">
                            <div className="b-blue-dark bs-contain br-no bp-center h-vh-13 w-vw-28 flex items-center flex-col mt-10 brad-8" style={{ backgroundImage: `url("${sideImages[0]}")` }}>
                                {/* <Button className="flex align-end justify-end" onClick={() => { inputSideFile.current.click() }}></Button> */}
                                {
                                    (sideImages[0] === '')
                                        ?
                                        <div className="w96 hs20 pa-vw-2 flex justify-end"></div>
                                        :
                                        <div className="w96 hs20 pa-vw-2 flex justify-end">
                                            <XIconBack height={20} width={20} onClick={removeSide[0]} />
                                        </div>
                                }
                                <CameraIcon height={20} width={20} onClick={() => { refs[0].current.click() }} />
                            </div>
                            <div className="b-blue-dark bs-contain br-no bp-center h-vh-13 w-vw-28 flex items-center flex-col mt-10 brad-8" style={{ backgroundImage: `url("${sideImages[1]}")` }}>
                                {
                                    (sideImages[1] === '')
                                        ?
                                        <div className="w96 hs20 pa-vw-2 flex justify-end"></div>
                                        :
                                        <div className="w96 hs20 pa-vw-2 flex justify-end">
                                            <XIconBack height={20} width={20} onClick={removeSide[1]} />
                                        </div>
                                }
                                <CameraIcon height={20} width={20} onClick={() => { refs[1].current.click() }} />
                            </div>
                            <div className="b-blue-dark bs-contain br-no bp-center h-vh-13 w-vw-28 flex items-center flex-col mt-10 brad-8" style={{ backgroundImage: `url("${sideImages[2]}")` }}>
                                {
                                    (sideImages[2] === '')
                                        ?
                                        <div className="w96 hs20 pa-vw-2 flex justify-end"></div>
                                        :
                                        <div className="w96 hs20 pa-vw-2 flex justify-end">
                                            <XIconBack height={20} width={20} onClick={removeSide[2]} />
                                        </div>
                                }
                                <CameraIcon height={20} width={20} onClick={() => { refs[2].current.click() }} />
                            </div>
                        </div>
                    </div>
                </div>
            </Box>
        </div>
    )
}

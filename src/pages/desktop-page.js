import React from 'react'
import { Pluses1 } from '../components/icons/pluses1'
import { Pluses2 } from '../components/icons/pluses2'
import { DesktopPeople } from '../components/icons/desktop-people'
import '../style/main.scss'

export const DesktopPage = () => {
    return (
        <div className="h-vh-100">
            <Pluses1 className="absolute ml-88 mt-307" />
            <Pluses2 className="absolute ml-v-80 mt-65" />
            <svg className="mt-vh-7 ml-88" width="75" height="38" viewBox="0 0 75 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M20.9452 20.3718C22.2965 17.8142 22.0543 13.1673 21.3496 11.1635C21.2217 10.7999 20.7627 10.8486 20.5652 11.1793C19.9037 12.2869 18.2037 14.2939 14.1713 15.5301C8.49923 17.269 4.68612 21.6171 2.01926 30.9033C1.92985 31.2146 2.16303 31.526 2.48564 31.526H20.457C20.7266 31.526 20.9452 31.7456 20.9452 32.0163V36M41.8137 8.25266C40.6569 5.36039 39.2055 3.42767 37.7365 2.11683C37.4356 1.8483 36.9734 2.07384 36.9396 2.47669C36.5703 6.87728 33.7434 10.0552 30.5898 13.2801C27.2065 16.7398 26.1324 19.3063 26.1324 22.3078C26.1324 28.279 30.9411 33.1195 36.8729 33.1195C42.8047 33.1195 47.6134 28.279 47.6134 22.3078C47.6134 19.5223 46.041 17.0184 44.6128 14.5985C44.4472 14.3178 44.0601 14.272 43.8355 14.5078L29.3546 29.7138M73 33.1808C70.3256 23.4653 66.0412 17.3687 61.2201 15.5914C57.6995 14.2935 55.7085 12.1458 54.8905 11.0816C54.6705 10.7955 54.214 10.7927 54.0342 11.1059C50.1085 17.9463 53.655 28.4933 53.655 33.1808" stroke="url(#paint0_linear)" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" />
                <defs>
                    <linearGradient id="paint0_linear" x1="2" y1="36" x2="51.4999" y2="4.51309" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#56CCF2" />
                        <stop offset="1" stop-color="#2F80ED" />
                    </linearGradient>
                </defs>
            </svg>

            <div className="flex flex-center">
                <svg className="mt-54" width="77" height="103" viewBox="0 0 77 103" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M15.3916 41.1038C12.9522 27.9377 19.6239 18.0075 43.0584 0C41.5231 16.5359 40.5262 26.0187 50.4356 24.7265C53.5097 29.8645 57.8134 37.2503 57.8134 50.0952C57.8134 62.9402 69.8023 56.5177 68.5733 43.6728C79.332 57.16 79.0251 72.2528 71.9548 80.602C64.8844 88.9512 66.4214 89.9146 68.2659 89.5934C70.1103 89.2723 73.4918 87.0244 73.4918 87.0244C55.9695 108.54 28.6102 106.934 9.24342 90.2357C9.24342 90.2357 9.85824 90.5568 11.3953 90.8779C12.9323 91.199 14.0708 89.7784 11.3953 87.0244C-1.25141 74.3373 -2.42513 64.95 8.01379 43.3517C8.01379 43.3517 7.47582 44.9573 8.62861 48.8108C9.78139 52.6642 17.831 54.2699 15.3916 41.1038Z" fill="url(#paint0_linear)" />
                    <defs>
                        <linearGradient id="paint0_linear" x1="77" y1="102.967" x2="6.12507" y2="87.0236" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#56CCF2" />
                            <stop offset="1" stop-color="#2F80ED" />
                        </linearGradient>
                    </defs>
                </svg>
                <div className="font-main c-blue fw-500 fs-36 lh-42 mt-42">Та манай вэбсайт руу утсаар нэвтэрнэ үү.</div>
                <div className="font-main c-blue bold fs-24 mt-8">Баярлалаа 😉 </div>

                <DesktopPeople className="mt-vh-25" />
            </div>
            <div>

            </div>
        </div>
    )
}
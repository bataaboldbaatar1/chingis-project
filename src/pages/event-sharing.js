import React, { useState, useEffect, useContext, useRef } from 'react'
import { useDoc, useCol, useFirebase } from '../Hooks/firebase';
// import { FacebookSharing } from '../components/facebook';
import { FormInput } from '../components/form-input';
import { AuthContext } from '../providers/auth-user-provider'
import { useKeyPress } from '../Hooks/key-press'
// import { Upvote } from '../components/upvote'
import { useHistory } from 'react-router-dom'
import { Layout, LeftIcon, RightIcon, CalendarIcon, Box, Stack, SendIcon } from '../components'
import { Upvote } from '../components/upvote';
import { Loader } from '../components/loader';

export const EventSharing = () => {
    let url = new URLSearchParams(window.location.search).get('id');
    const { user } = useContext(AuthContext);
    const history = useHistory();
    const {firebase} = useFirebase();
    const lastComment = useRef(null)
    const [desc, setDesc] = useState(false)
    const [userLocal, setUserLocal] = useState(null);
    const { data } = useDoc(`Events/${url}`);
    // const { data: data2 } = useDoc(`users/${data && data.createdUser}`);
    const { data: tenderInfo } = useDoc(`Tenders/${data && data.tender}`);
    const [showComments, setShowComment] = useState(false);
    const [indx, setIndx] = useState(0);

    const { createRecord, data: data3, loading: commentLoading } = useCol(`Events/${url}/Comments`, true);
    const { data: sideImageUrls, loading: imageLoading } = useCol(`Events/${url}/SideImageUrls`);

    const [allImageUrls, setAllImageUrls] = useState([]);
    const submit = async () => {
        const id = randomStringAndNumber();
        if (comment !== '') {
            await createRecord(userLocal.uid + "--" + id, {
                comment: comment,
                username: data2.username,
                uid: userLocal.uid,
                eventId: url,
            });
            console.log(lastComment)
            lastComment && lastComment.current.scrollIntoView({ behavior: 'smooth', block: 'center' })
        }
        setComment('')
    }

    // useKeyPress("Enter", submit);

    const getDate = (time) => {
        time = time.toDate().toString().split(' ')
        if(time[1] == "Dec") {
            return (time[3] + " 12 " + time[2])
        }
        if(time[1] == "Jan") {
            return (time[3] + " 1 " + time[2])
        }
        if(time[1] == "Feb") {
            return (time[3] + " 2 " + time[2])
        }
        if(time[1] == "Mar") {
            return (time[3] + " 3 " + time[2])
        }
        if(time[1] == "Apr") {
            return (time[3] + " 4 " + time[2])
        }
        if(time[1] == "May") {
            return (time[3] + " 5 " + time[2])
        }
        if(time[1] == "Jun") {
            return (time[3] + " 6 " + time[2])
        }
        if(time[1] == "Jul") {
            return (time[3] + " 7 " + time[2])
        }
        if(time[1] == "Aug") {
            return (time[3] + " 8 " + time[2])
        }
        if(time[1] == "Sep") {
            return (time[3] + " 9 " + time[2])
        }
        if(time[1] == "Oct") {
            return (time[3] + " 10 " + time[2])
        }
        if(time[1] == "Nov") {
            return (time[3] + " 11 " + time[2])
        }
    }

    const isLoaded = (...loaders) => loaders.reduce((condition, loader) => condition || loader, false);

    useEffect(() => {
        if (user) {
            setUserLocal(user);
        }
    }, [user]);

    useEffect(() => {
        if (sideImageUrls && data) {
            setAllImageUrls([{ url: data.mainImageUrl }, ...sideImageUrls])
        }

    }, [data, sideImageUrls])

    const { data: data2 } = useDoc(`users/${userLocal && userLocal.uid}`);
    const { data: postInfo } = useDoc(`users/${data && data.createdUser}`);


    const [comment, setComment] = useState('');
    const handleChangeComment = (e) => setComment(e.target.value);


    const randomStringAndNumber = () => {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < 7; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    if (isLoaded(commentLoading, imageLoading)) {
        return (
            <div className='flex-center w100 h-vh-90'>
                <Loader height={125} width={125} />
            </div>
        )
    }


    return (
        <Layout color={true} className='font-main'>
            <Stack size={2}>
                <Box className='pb-15' type="bottom">

                    <div className="flex-row pb-16 pa-10 items-center">
                        <div className="pr-16 mr-10" style={{ backgroundColor: 'gray', backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundImage: `url("${data2 && data2.profileImageUrl}")`, height: 28, width: 28, borderRadius: 50 }} ></div>
                        <p className="m-0 ib fs-18 fw700">{data2 && data2.username}</p>
                    </div>
                    <div >
                        <div style={{ backgroundColor: 'gray', backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundImage: `url("${allImageUrls && allImageUrls[indx].url}")`, paddingTop: '80%' }} className='w-vw-100 flex justify-between bradius-8 items-center pr'>
                            {
                                indx !== 0 ? <LeftIcon className='absolute' style={{ top: '50%', left: '0' }} width={30} height={30} color={'#FFFFFF'} onClick={() => { setIndx((indx) => indx - 1); console.log(allImageUrls[indx].url) }} />
                                    : <div></div>
                            }
                            {
                                (allImageUrls && indx !== allImageUrls.length - 1) ? <RightIcon className='absolute' style={{ top: '50%', right: '0' }} width={30} height={30} color={'#FFFFFF'} onClick={() => { setIndx((indx) => indx + 1); console.log(allImageUrls[indx].url) }} />
                                    : <div></div>
                            }
                        </div>
                        <div className='flex mt-10 justify-center '>
                            {
                                indx === 0 ?
                                    <div className='b-blue w-10 h-10 mr-8 circle'></div>
                                    :
                                    <div className='b-dark-40 w-10 h-10 mr-8 circle' onClick={() => { setIndx(0) }} ></div>

                            }
                            {
                                indx === 1 ?
                                    <div className='b-blue w-10 h-10 mr-8 circle'></div>
                                    :
                                    <div className='b-dark-40 w-10 h-10 mr-8 circle' onClick={() => { setIndx(1) }} ></div>

                            }                            {
                                indx === 2 ?
                                    <div className='b-blue w-10 h-10 mr-8 circle'></div>
                                    :
                                    <div className='b-dark-40 w-10 h-10 mr-8 circle' onClick={() => { setIndx(2) }} ></div>

                            }                            {
                                indx === 3 ?
                                    <div className='b-blue w-10 h-10 mr-8 circle'></div>
                                    :
                                    <div className='b-dark-40 w-10 h-10 mr-8 circle' onClick={() => { setIndx(3) }} ></div>

                            }
                        </div>
                    </div>
                    <div className="margin-auto w-vw-90">
                        <div>
                            <div className="ma-0 flex-row items-center pb-20 pt-10 fs-18">
                                <div className='c-blue bold fs-24 fw700'>{data && data.vote + "  "} Санал</div>
                                <div className='ma-0'>/{data && data.voteCount + "  "}Санал</div>
                            </div>
                            <div>
                                <div className='pr '>
                                    <div className='w100 absolute bradius-10 b-gray2 op30 h-10'></div>
                                    <div className='absolute w100 bradius-10 primary-gra h-10 ' style={{ width: `${(data && data.vote / data.voteCount) * 100 || 0}%` }}></div>
                                </div>
                            </div>
                        </div>

                        <div className="mt-30 flex items-center">
                            <CalendarIcon className="pa-5" width={25} height={25} />
                            <div className='ml-15 flex-col h-70 justify-around'>
                                <p className='ma-0'>Эхлэх хугацаа  {getDate(data && data.createdAt)}</p>
                                <b>Дуусах хугацаа  {data && data.endsAt.split('-').join(' ')}</b>
                            </div>
                        </div>
                        <div className='w100'>
                            {
                                tenderInfo && tenderInfo.name ?
                                    <div className='w100 flex justify-start'>
                                        <div className="flex-row pb-16 pa-10 items-center" onClick={() => { history.push(`/tender-id?id=${data && data.tender}`) }}>
                                            <div className="pr-16 mr-10" style={{ backgroundColor: 'gray', backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundImage: `url("${data2 && data2.profileImageUrl}")`, height: 20, width: 20, borderRadius: 50 }} ></div>
                                            <p className="m-0 ib fs-14">{data2 && data2.username} оролцох {tenderInfo.name}</p>
                                        </div>
                                    </div>
                                    :
                                    <></>

                            }
                        </div>

                        <div className='flex-row flex-wrap'>
                            {
                                data && data.categories.map((e, i) => {
                                    return (
                                        <div key={i} className='pa-6 ma-8 br-fb-color-1 bradius-10 flex-center'>
                                            <p className='ma-0 wbreak c-fb-color'>{e}</p>
                                        </div>
                                    )
                                })
                            }
                        </div>

                    </div>
                </Box>

                <Box className='pt-15 pb-15'>
                    <div className="margin-auto w-vw-90">

                        <div className="w100">
                            <h1 className='fw700 fs-24' >{data && data.name}</h1>
                            {
                                (!desc) ?
                                    data && data.desc.length > 150 ?
                                        <>
                                            <p className="wbreak">{data.desc.substring(0, 150)}<span className='fw700 ma-0' onClick={() => { setDesc(true) }}>...see more</span></p>
                                        </>
                                        :
                                        <p className="wbreak fw400 fs-15">{data.desc}</p>
                                    :
                                    <p className="wbreak fw400 fs-15">{data && data.desc}</p>
                            }
                        </div>
                    </div>
                    <div className='w100 mt-30 h-5 b-dark-40'></div>
                    <div className="margin-auto w-vw-90">
                        <div className="w100">
                            <h1 className='fw700 fs-18' >Байршил</h1>
                            <p className="wbreak">{data && data.location}</p>
                        </div>
                    </div>
                </Box>

                {/* <div className='w-vw-100 margin-auto p-f'>
                        {
                            (JSON.stringify(data && data.createdUser) === JSON.stringify(user && user.uid)) ?
                                <button disabled className='w-vw-90 h-44 b-white br-gra bradius-5'>
                                    <p className="c-gra bold">Таны аян</p>
                                </button>
                                :
                                !user ?
                                    <button disabled className='b-white br-gra bradius-5'>
                                        <p className="c-gra bold">Нэвтрэх хэрэгтэй</p>
                                    </button>
                                    :
                                    tenderInfo && tenderInfo.name ?
                                    <button className='b-white br-gra bradius-5' onClick={() => { history.push(`/addEvent?tender=${url}`) }}>
                                            <p className="c-gra bold">Оролцох</p>
                                        </button>
                                    :
                                    <div>
                                        <Upvote eventId={data.id} date={data.createdAt} uid={data.createdUser} projectName={data.name} />
                                    </div>
                        }
                    </div> */}



                <Box comment={true}>
                    <div className="margin-auto w-vw-90">
                        <div className='flex items-center justify-between flex-row'>
                            <h3>Сэтгэгдэл</h3>
                            <div className='c-dark'>{data3 && data3.length} Сэтгэгдэл</div>
                        </div>

                        {
                            !user ?
                                <div className='flex justify-between'>
                                    <FormInput className="pa-7 w-vw-80 br-primary-1 br-b-primary bradius-40" value={comment} placeholder="Нэвтрэх хэрэгтэй"></FormInput>
                                    <SendIcon onClick={submit} width={24} height={30} />
                                </div>

                                :
                                <div className='flex justify-between'>
                                    <FormInput onChange={handleChangeComment} className="pa-7 w-vw-80 outl h-36 nb b-dark-40 bradius-10" value={comment} placeholder="Сэтгэгдэл үлдээх..."></FormInput>
                                    <SendIcon onClick={submit} width={24} height={30} />
                                </div>
                        }

                        <div className='mb-60'>
                            {
                                data3 && data3.map((e, i) => {
                                    if (i < 2) {
                                        return (
                                            <div key={i} className="pl-7 mt-10 mb-20">
                                                <h4 className="ma-0 mb-8">{e.username}</h4>
                                                <p className="ma-0 break-wrap">{e.comment}</p>
                                            </div>
                                        )
                                    }
                                    return '';
                                })
                            }
                            {
                                (showComments) ?
                                    data3 && data3.map((e, i) => {
                                        if (i >= 2) {
                                            return (
                                                <div key={i} className="pl-7 mt-10 mb-20">
                                                    <h4 className="ma-0 mb-8">{e.username}</h4>
                                                    <p className="ma-0 break-wrap">{e.comment}</p>
                                                </div>
                                            )
                                        }
                                        return '';
                                    })
                                    :
                                    data3 && data3.length > 2 ?
                                        <div className='w100 justify-center flex'>
                                            <div className="mt-10 mb-20 ul c-loading" onClick={() => { setShowComment(true) }}>Бүх сэтгэгдэл харах</div>
                                        </div>
                                        :
                                        <></>


                            }
                        </div>

                        <div className='mt-100' ref={lastComment} ></div>

                        <div className='w-vw-90 h-44 bottom-40 margin-auto p-f z-i bradius-5'>
                            {
                                (JSON.stringify(data && data.createdUser) === JSON.stringify(user && user.uid)) ?
                                    <button disabled className='w100 b-white br-fb-color-2 bradius-5'>
                                        <p className="c-gra bold">Таны аян</p>
                                    </button>
                                    :
                                    !user ?
                                        <button disabled className='w100 b-white br-fb-color-2 bradius-5'>
                                            <p className="c-gra bold">Нэвтрэх хэрэгтэй</p>
                                        </button>
                                        :
                                        tenderInfo && tenderInfo.name ?
                                            <button className='w100 b-white br-fb-color-2 bradius-5' onClick={() => { history.push(`/addEvent?tender=${url}`) }}>
                                                <p className="c-gra bold">Оролцох</p>
                                            </button>
                                            :
                                            <div>
                                                <Upvote eventId={data.id} date={data.createdAt} uid={data.createdUser} projectName={data.name} />
                                            </div>
                            }
                           
                        </div>

                    </div>

                </Box>


            </Stack>
        </Layout>
    )
}
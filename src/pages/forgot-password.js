import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom'
import { AuthContext } from '../providers/auth-user-provider';
import { FormInput, Layout, Button } from '../components/index'
import { useFirebase } from '../Hooks';

export const ForgotPass = () => {
    const [email, setEmail] = useState('');
    const { auth } = useFirebase();
    const history = useHistory();
    const { user } = useContext(AuthContext);
    const [sent, setSent] = useState(false)

    const Send = () => {
        auth.sendPasswordResetEmail(email).then(() => {
            // Email sent.
            console.log("Sent")
            setSent(true)
        }).catch(function (error) {
            console.log(error)
        });
    }

    console.log(user);

    if (user) {
        history.push('/feed')
    }

    const handleChangeEmail = (e) => setEmail(e.target.value);

    return (
        <Layout>
            <div className='container margin-auto flex-center'>
                <div className='w-327'>
                    {sent ? 
                        <div className='flex-center'>
                            <p className='title'>Майл явуулсан</p>
                            <p className='c-dark w-281 mb-64'>Бид нууц үг солих линкийг таний Gmail хаягруу явуулсан.</p>
                            {/* <FormInput className='w-vw-87 brad-5 h-50 pl-20 bb-lightgray-1 boxs mt-86 mb-16 br-dark-1' label='Цахим хаяг' placeholder='name@mail.domain' value={email} onChange={handleChangeEmail} /> */}
                            <FormInput className="bradius-5 pl-44 bb-lightgray-1 boxs mb-16 h-50 br-dark-1" label='Цахим хаяг' placeholder='Gmail' type='email' value={email} onChange={handleChangeEmail} />
                            <div className='mt-4 center'></div>
                            {
                                email === '' ?
                                    // <ActionButton icon='popup' trigger={<ActionButton disabled>Илгээх</ActionButton>} disabled >Илгээгдсэн!</ActionButton>
                                    <Button className="font-main h-50 c-primaryfourth third-gradient brad-5" disabled>Илгээх</Button>
                                    :
                                    <Button className="font-main h-50 c-primaryfourth third-gradient brad-5" onClick={Send}>Илгээх</Button>
                                    // <ActionButton icon='popup' trigger='Илгээх' onClick={Send} >Илгээгдсэн!</ActionButton>
                            }
                        </div>
                    :
                        <div className='flex-center text-center'>
                            <p className='title'>Нууц үгээ мартсан?</p>
                            <p className='c-dark w-281 mb-64'>Та И-мейл хаягаа оруулна уу, бид нууц үг солих линкийг тань руу явуулна.</p>
                            {/* <FormInput className='w-vw-87 brad-5 h-50 pl-20 bb-lightgray-1 boxs mt-86 mb-16 br-dark-1' label='Цахим хаяг' placeholder='name@mail.domain' value={email} onChange={handleChangeEmail} /> */}
                            <FormInput className="bradius-5 pl-44 bb-lightgray-1 boxs mb-16 h-50 br-dark-1" label='Цахим хаяг' placeholder='И-мейл' type='email' value={email} onChange={handleChangeEmail} />
                            <div className='mt-4 center'></div>
                            {
                                email === '' ?
                                    // <ActionButton icon='popup' trigger={<ActionButton disabled>Илгээх</ActionButton>} disabled >Илгээгдсэн!</ActionButton>
                                    <Button className="font-main h-50 c-primaryfourth third-gradient brad-5" disabled>Илгээх</Button>
                                    :
                                    <Button className="font-main h-50 c-primaryfourth third-gradient brad-5" onClick={Send}>Илгээх</Button>
                                    // <ActionButton icon='popup' trigger='Илгээх' onClick={Send} >Илгээгдсэн!</ActionButton>
                            }
                        </div>
                    }
                </div>
            </div>
        </Layout>
    )
}

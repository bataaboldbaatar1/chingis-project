import React, { useState, useContext } from 'react';
import { SearchInput, TenderCampaign, ShowTender, Layout } from '../components'
import { ShowPosts } from '../components/showposts'
import { useCol, useDoc } from '../Hooks/firebase';
import { AuthContext } from '../providers/auth-user-provider'
import { Loader } from '../components/loader'

// import { useHistory } from 'react-router-dom'

export const HomeDefault = () => {
    // const history = useHistory();
    // console.log(history)
    const { user } = useContext(AuthContext);
    const [search, setSearch] = useState('')
    const [flag, setFlag] = useState(false);
    let uid;

    if (user) {
        uid = user.uid;
    }

    const { loading: userLoading } = useDoc(`users/${uid}`);
    const isLoaded = (...loaders) => loaders.reduce((condition, loader) => condition || loader, false);

    const { data: events } = useCol('Events');
    const { data: tenders } = useCol('Tenders');

    const toggle = () => setFlag((flag) => !flag);

    const includes = (item, search) => (item.name && item.name.toLowerCase().includes(search));

    if (isLoaded(userLoading)) {
        return (
            <div className='flex-center w100 h-vh-90'>
                <Loader height={125} width={125}/>
            </div>
        )
    }

    return (
        <Layout>
            <div className="primary-gradient bradius-btm-r bradius-btm-l">
                <SearchInput type="notAddEvent" updateSearch={setSearch} placeholder='Ангилал хайх' />
                <TenderCampaign toggle={toggle} />
            </div>
            <div className="container-bruh">
                {flag && <ShowPosts data={events.filter((event) => includes(event, search))} />}
                <div className="mt-5 mb-5"></div>
                {!flag && <ShowTender data={tenders.filter((tender) => includes(tender, search))} />}
            </div>
        </Layout>
    )
}
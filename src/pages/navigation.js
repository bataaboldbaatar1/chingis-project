import React, {useContext} from 'react'
import { DropdownMenu, ArrowIcon, Grid, DropdownBlue } from '../components/';
import { useHistory } from 'react-router-dom';
import _ from 'lodash';
import { PeopleWithText, WhiteArrowIcon } from '../components'
import { TeamLogo } from '../components/icons/team-logo';
import { SignUpContext } from '../providers/sign-up-provider.jsx';

export const Navigation = ({ children }) => {

    let history = useHistory();

    return (
        <>
            {
                (history.location.pathname === '/forgot-pass') ?
                    <Grid className='h-70 shadow3 w100' columns="3">
                        <div className='flex items-center justify-start'>
                            {
                                !_.isEmpty(
                                    _.chain([
                                        '/forgot-pass',
                                    ])
                                        .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                        .value()
                                )
                                && <ArrowIcon className="ml-20" onClick={() => { history.goBack() }} width={20} height={20} />
                            }

                        </div>
                        <div className='flex items-center justify-center'>
                        </div>
                        <div className='flex items-center justify-end'>
                            {
                                !_.isEmpty(
                                    _.chain([
                                        '/forgot-pass'
                                    ])
                                        .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                        .value()
                                )
                                && <div className="c-fb-color font-main mr-20" onClick={() => history.push('/feed')}>Алгасах</div>
                            }

                        </div>
                    </Grid>
                :
                    (history.location.pathname === '/login' || history.location.pathname === '/register') ?
                        <div></div>
                        :
                        <Grid className='h-70 shadow3 primary-gradient w100' columns="3">
                            <div className='flex items-center justify-start'>
                                {
                                    !_.isEmpty(
                                        _.chain([
                                            '/feed',
                                        ])
                                            .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                            .value()
                                    )
                                    && <TeamLogo className="ml-15" width='20' onClick={() => { history.push("/feed") }} />
                                }

                                {
                                    !_.isEmpty(
                                        _.chain([
                                            '/event-id',
                                            '/search',
                                            '/tender-id',
                                            "/verify-account",
                                            "/profile",
                                            '/vote-history',
                                            '/forgot-pass',
                                        ])
                                            .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                            .value()
                                    )
                                    && <ArrowIcon className="ml-20" onClick={() => { history.goBack() }} width={20} height={20} />
                                }

                            </div>
                            <div className='flex items-center justify-center'>
                            </div>
                            <div className='flex items-center justify-end'>
                                {
                                    !_.isEmpty(
                                        _.chain([
                                            '/feed',
                                            '/event-id',
                                            '/search',
                                            '/tender-id',
                                            '/profile'
                                        ])
                                            .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                            .value()
                                    )
                                    && <DropdownMenu ></DropdownMenu>
                                }

                                {
                                    !_.isEmpty(
                                        _.chain([
                                            '/register',
                                            '/login',
                                            '/forgot-pass'
                                        ])
                                            .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                            .value()
                                    )
                                    && <div className="c-white font-main mr-20" onClick={() => history.push('/feed')}>Алгасах</div>
                                }

                            </div>
                        </Grid>
            }
        </>
    )
}
export const WhiteNav = () => {
    const history = useHistory();
    return (
        <>
            {
                (history.location.pathname === '/login' || history.location.pathname === '/register') ?
                    <div></div>
                    :
                    <Grid className='h-70 shadow3 box-bottom b-white w100' columns="3">
                        <div className='flex items-center justify-start'>
                            {
                                !_.isEmpty(
                                    _.chain([
                                        '/feed',
                                    ])
                                        .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                        .value()
                                )
                                && <div className="font-main ml-10 fs-24 lh-43 c-white" onClick={() => { history.push("/feed") }}>Дөл</div>
                            }
                            {
                                !_.isEmpty(
                                    _.chain([
                                        '/event-id',
                                        '/search',
                                        '/tender-id',
                                        "/verify-account",
                                        "/profile",
                                        '/vote-history',
                                    ])
                                        .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                        .value()
                                )
                                && <ArrowIcon className="ml-20" onClick={() => { history.goBack() }} width={20} height={20} />
                            }
                        </div>
                        <div className='flex items-center justify-center'>
                                {
                                    !_.isEmpty(
                                        _.chain([
                                            '/profile',
                                        ])
                                            .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                            .value()
                                    )
                                    && <div className='items-center flex ml-10'>
                                        <span className='font-main c-blue bold fs-20'>Профайл</span>
                                    </div>
                                }
                                {
                                    !_.isEmpty(
                                        _.chain([
                                            '/vote-history',
                                        ])
                                            .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                            .value()
                                    )
                                    && <div className='items-center flex'>
                                        <span className='font-main c-blue bold fs-20'>Түүx</span>
                                    </div>
                                }
                            
                        </div>
                        <div className='flex items-center justify-end'>
                            {
                                !_.isEmpty(
                                    _.chain([
                                        '/feed',

                                        '/profile'
                                    ])
                                        .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                        .value()
                                )
                                && <DropdownBlue></DropdownBlue>
                            }
                            {
                                !_.isEmpty(
                                    _.chain([
                                        '/register',
                                        '/login'
                                    ])
                                        .filter(path => path === '/feed' ? history.location.pathname === '/feed' : history.location.pathname.match(path))
                                        .value()
                                )
                                && <div className="c-white font-main mr-20" onClick={() => history.push('/feed')}>Алгасах</div>
                            }
                        </div>
                    </Grid>
            }
        </>
    )
}
export const SignUpNavbar = () => {
    const history = useHistory();
    const { prevStep, step } = useContext(SignUpContext);

    return (
        <div className="primary-gradient w-vw-100 mb-48 h-vh-60">
            <div className="w-vw-90 margin-auto h-vh-10 flex items-center justify-between">
                <WhiteArrowIcon width={20} height={20} onClick={() => {prevStep()}} />
                <div className="c-white font-main" onClick={() => history.push('/feed')}>Алгасах</div>
            </div>
            {
                history.location.pathname === '/register'
                    ?
                    <div className="mb-39">
                        <div className="font-main c-white bold fs-24">Бүртгүүлэх</div>
                        <div className="c-white font-Raleway fs-16 normal mt-24">Та өөрийн бүртгэлээ үүсгэнэ үү?</div>
                    </div>
                    :
                    <div className="mb-39">
                        <div className="font-main c-white bold fs-24">Нэвтрэх</div>
                        <div className="c-white font-Raleway fs-16 normal mt-24">Та өөрийн бүртгэлээр орно уу?</div>
                    </div>
            }
            <PeopleWithText />
        </div>
    )
}


import React, { useEffect, useContext, useRef, useState } from 'react';
import { useDoc, useCol, useFirebase } from '../Hooks/firebase';
import { AuthContext } from '../providers/auth-user-provider';
import { useStorage } from '../Hooks';
import { GmailIcon, Layout, Modal, ModalSlider } from '../components';
import {CallIcon, EditIcon} from '../components/icons'
import { useHistory } from 'react-router-dom';
import { Stack, Box } from '../components';
import { RenderProfilePost } from '../components/render-profile-post';
import { Loader } from '../components/loader';
import { Verify } from './verify-account';
import { WhiteNav } from './navigation'
// import { VerifiedIcon } from '../components/icons/verified-icon';
// import { app } from 'firebase';

const Profile = (vote, voteCount) => {
    let oid = new URLSearchParams(window.location.search).get('user');
    const { user } = useContext(AuthContext)
    const { uid } = user || {};
    const history = useHistory();
    const { data, updateRecord, loading: eventLoading } = useDoc(`users/${oid ? oid : uid}`);
    const { data: createdPosts } = useCol(`users/${oid ? oid : uid}/createdEvents`);
    const { data: allEventInfo, deleteRecord } = useCol(`Events`)
    const { data: userData } = useDoc(`users/${uid}`);
    const { profileImage, username } = data || {};
    const [file, setFile] = useState('')
    const [imgSrc, setImgSrc] = useState('')
    const [appear, setAppear] = useState(false);
    const [show, setShow] = useState(false);
    const [showPfp, setShowPfp] = useState(false);
    const inputFile = useRef(null);
    const { firebase } = useFirebase();
    const profileImageSrc = useStorage(`profileImages/${oid ? oid : uid}/profileImage.jpg`)

    const gmailShow = () => {

        setShow(!show)
    }

    const callShow = () => {

        setAppear(!appear)
    }

    const pfpShow = () => {
        setShowPfp(!showPfp);
    }

    useEffect(() => {
        if (inputFile.current) {
            function onFileChange() {
                setFile(inputFile.current.files[0]);
                console.log(inputFile)
                setImgSrc(URL.createObjectURL(inputFile.current.files[0]))
            }

            inputFile.current.addEventListener('change', onFileChange);
        }
    }, []);

    useEffect(() => {
        if (file) {
            console.log("aafd")
            var storageSideRef = firebase.storage().ref().child(`profileImages/${uid}/profileImage.jpg`);
            storageSideRef.put(inputFile.current.files[0])
                .then((snapshot) => {
                    console.log('Done. Storage');
                    updateRecord({ profileImage: "img" })
                    console.log('Done. Firestore')
                })
        }
    }, [file, firebase, uid, updateRecord])

    if (eventLoading) {
        return <div className='flex-center w100 h-vh-90'>
            <Loader height={125} width={125}/>
        </div>
    }
    console.log(data)

    return (
        <div className='h-vh-100 tempCol-0-4 b-background'>
            <div className="flex flex-col b-background">
                <WhiteNav />
                <Stack size={5} className="b-background">
                    <Box className="flex justify-center pv-20 mt-10 box-top pa-10" type='bottom'>
                        <div onClick={pfpShow} style={{ borderRadius: '100%', backgroundColor: '#9C9C9C', backgroundSize: 'cover', backgroundImage: `url("${imgSrc === '' ? profileImageSrc : imgSrc}")` }} className='flex-center bradius-10 w-100 h-100 margin-auto' >
                            <p className='op fs-24 text-center font-main'>{(profileImage === "default") && (imgSrc === '') ? username[0] : ''}</p>
                            <EditIcon className=" absolute mt-65 ml-75" width={75} height={75} />
                        </div>
                        <div className="flex-center align-center">
                            <h3 className="lh-31 fs-24 m-10 mr-20 ml-20 font-main">{(data && data.username) || ''}</h3>
                            {

                            }
                        </div>

                        <input type='file' id='file' ref={inputFile} style={{ display: 'none' }} />

                        <input type='file' id='file' ref={inputFile} style={{ display: 'none' }} />
                        <Stack size={3}>
                            {((userData && userData.logged === 'Group') || (userData && userData.id === uid)) &&
                                <div className="flex-row justify-between space-between">
                                    <div onClick={gmailShow} className="flex-center btn-pf font-main bradius-10 bold fs-14 h-32">Емайл</div>
                                    <div onClick={callShow} className="flex-center btn-pf font-main bradius-10 bold fs-14 h-32">Утас</div>
                                    <div onClick={() => { history.push('/vote-history') }} className="flex-center btn-pf font-main bradius-10 bold fs-14 h-32">Түүх</div>
                                </div>
                            }
                            <Modal show={show} closeModal={(show) => setShow(!show)}>
                                <div className="flex flex-center">
                                    <div className="flex flex-center fs-16 font-main t-45 w-vw-90 bshadow bradius-10 b-default">
                                        <div className="text-center font-main mt-5 fw700 mb-10 bt-primary-1">Емайл</div>
                                        <div className="bt-gray4-1 w100 flex-row flex">
                                            <GmailIcon width={16} height={16} className=" flex flex-center pa-25 mt-4" />
                                            <div className=" flex flex-center text-center font-main fs-16">{user && user.email}</div>
                                        </div>
                                    </div>
                                </div>
                            </Modal>
                            <Modal show={appear} closeModal={(appear) => setAppear(!appear)}>
                                <div className="flex flex-center">
                                    <div className="flex flex-center fs-16 font-main t-45 w-vw-90 bshadow bradius-10 b-default">
                                        <div className="text-center font-main mt-5 fw700 mb-10 bt-primary-1">Утас</div>
                                        <div className="bt-gray4-1 w100 flex-row flex items-center">
                                            <CallIcon width={16} height={16} className="pa-25" />
                                            <div className="fs-16">{userData && userData.phone || "Бүртгэлтэй утасны дугаар алга."}</div>
                                        </div>
                                    </div>
                                </div>
                            </Modal>
                            <ModalSlider show={showPfp} closeSlider={(showPfp) => { setShowPfp(!showPfp) }}>
                                {!oid &&
                                    <div className="flex flex-center">
                                        <div className="h-44 lh-20 fs-16 fw700 font-main b-default w-vw-90 brad-top-10 brad-bottom-10 text-center pa-10 c-gray">Нүүр зураг үзэх</div>
                                        <div onClick={() => { inputFile.current.click() }} className="h-44 lh-20 fs-16 font-main mt-8 b-default w-vw-90 brad-bottom-10 brad-top-10 text-center pa-10 c-blue">Нүүр зураг солих</div>
                                    </div>
                                }
                            </ModalSlider>
                        </Stack>
                    </Box>
                    {
                        userData && userData.logged === 'Group' && 
                            userData && userData.verified === 'false' || '' ?
                            <Box className="flex pv-20 justify-center">
                                <div>
                                    <p className="font-main fs-16">Та өөрийн компании хаягаа баталгаат хаяг болгоx бол <span className="c-primary" onClick={() => { history.push('/verify-account')}}>Энд дарна</span> уу !</p>
                                </div>
                            </Box>
                            :
                            <></>
                    }
                    {
                        allEventInfo && allEventInfo.filter((event) => !createdPosts.every((post) => post.id !== event.id)).map((e) => <RenderProfilePost key={e.id} {...e} deleteEvent={deleteRecord} />)
                    }
                </Stack>
            </div>
        </div>
    )
}
export default Profile;

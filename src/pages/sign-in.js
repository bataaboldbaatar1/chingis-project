import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom'
import { AuthContext } from '../providers/auth-user-provider';
import { useFirebase } from '../Hooks/firebase';
import { Layout, FormInput, ActionButton, Stack, ArrowIcon } from '../components'
import { SignUpNavbar } from './navigation'
import { Loader } from '../components/loader'
import { SignUpContext } from '../providers/sign-up-provider.jsx';
import {ProgressLogin} from '../components/progress-bar'


export const SignIn = () => {
    const {nextStep, step, setError, error } = useContext(SignUpContext);
    const history = useHistory();
    const { user } = useContext(AuthContext);
    const { auth, firebase } = useFirebase();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [showLoginForm, setShowLoginForm] = useState(false);
    console.log(showLoginForm)

    // const { user } = useContext(AuthContext)
    // const [state, setState] = useState({ email: '', password: '', password2: '', username: '', phone: '', groupName: '', groupMail: '', medku: '' });
    // const history = useHistory();
    // const [error, setError] = useState('');
    // const { firebase, auth, firestore } = useFirebase();

    // const signUpPage = () => {
    //     history.push('./register')
    // }
    console.log(user);

    if (user) {
        history.push('/feed')
    }

    const handleChangeEmail = (e) => setEmail(e.target.value);
    const handleChangePassword = (e) => setPassword(e.target.value);

    // const handleChangeEmail = (e) => setState({ ...state, email: e.target.value });
    // const handleChangePassword = (e) => setState({ ...state, password: e.target.value });


    const signIn = async () => {
        await auth.signInWithEmailAndPassword(email, password).catch((error) => {
            setError('И-мэйл эсвэл нууц үг буруу байна');
        })
    }

    // const facebook = () => {
    //     var provider = new firebase.auth.FacebookAuthProvider();

    //     auth.signInWithPopup(provider).then(function(result) {
    //         var token = result.credential.accessToken;
    //         var user = result.user;
    //         console.log(token);
    //         console.log(user);
    //     }).catch(function(error) {
    //         var errorMessage = error.message;
    //         alert(errorMessage)
    //     });
    // }

    const google = () => {
        var provider = new firebase.auth.GoogleAuthProvider();


        auth.signInWithPopup(provider).then((result) => {
            var token = result.credential.accessToken;
            var user = result.user;

            console.log(token);
            console.log(user);

        }).catch(function (error) {
            var errorMessage = error.message;

            console.log(errorMessage)
        });
    }

    const isFilled = () => {

        if (email === '')
            return true;
        if (password === '')
            return true;

        return false
    }

    // if (!user) {
    //     return (
    //         <div className='flex-center w100 h-vh-90'>
    //             <Loader height={125} width={125}/>
    //         </div>
    //     )
    // }

    return (
        // <Layout>
        //     <div className='items-center'>
        //         <div>
        //             <p className='title'>Нэвтрэх</p>
        //             <FormInput label='Цахим хаяг' placeholder='name@mail.domain' value={email} onChange={handleChangeEmail} />
        //             <div className='mt-4'></div>
        //             <FormInput label='Нууц үг' type='password' placeholder='Password' value={password} onChange={handleChangePassword} />
        //             <ActionButton onClick={signIn}>Нэвтрэх</ActionButton>
        //         </div>
        //         <div className='mt-30'>
        //             <ActionButton onClick={facebook}>Facebook</ActionButton>
        //             <ActionButton onClick={google}>Google</ActionButton>
        //         </div>
        //     </div>
        // </Layout>
        <Layout>
            <div className="font-main w-vw-100 text-center">
                <div className="flex-col justify-between">
                    {!showLoginForm &&
                        <div className="w-vw-100 margin-auto">
                             <ProgressLogin step={step} />
                            <SignUpNavbar />
                            <Stack size={4} className="w90">
                                <ActionButton className="font-main mt-6 h-50 c-primaryfourth brad-5 pa-12" icon='google' onClick={google}>Google хаягаар нэвтрэх</ActionButton>
                                <ActionButton className="font-main h-50 c-blue b-white brad-5 shadow3" icon="white" onClick={() => { setShowLoginForm(true) }}>И-мейл хаягаар нэвтрэх</ActionButton>
                                <div className="flex justify-center">
                                    <div className="font-main c-dark">Бүртгэл байхгүй ?</div>
                                    <div className="ml-5 text-button c-blue bold" onClick={() => history.push('/register')}>Бүртгүүлэх </div>
                                </div>
                            </Stack>
                        </div>
                    }

                    {showLoginForm &&
                        <div>
                            <ProgressLogin step={2} />
                            <div className="">
                                <div className="w-vw-90 margin-auto h-vh-10 flex items-center justify-between">
                                    <ArrowIcon width={15} height={15} onClick={() => { setShowLoginForm(false) }} />
                                    <div className="c-blue font-main" onClick={() => history.push('/feed')}>Алгасах</div>
                                </div>
                            </div>
                            <div className="mb-90 ">
                                <div className="font-main  bold fs-24">Нэвтрэх</div>
                                <div className="c-dark font-Raleway fs-16 normal mt-24">Та өөрийн бүртгэлээр нэвтэрнэ үү?</div>
                            </div>
                            <div className="w90">
                                <Stack size={4} className="">
                                    <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Цахим хаяг' placeholder='name@mail.com' type='email' value={email} onChange={handleChangeEmail} />
                                    <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Нууц үг' placeholder='Password' type='password' value={password} onChange={handleChangePassword} />
                                    <ActionButton className="font-main h-50 c-primaryfourth third-gradient brad-5" disabled={isFilled()} icon="signupin" onClick={signIn}>Нэвтрэх</ActionButton>
                                    {error && <div className="c-red fs-14">{error}</div>}
                                    <div className="flex justify-center">
                                        <div className='font-main'>Нууц үг мартсан?</div>
                                        <div onClick={() => { history.push('./forgot-pass') }} className="ml-5 text-button c-blue bold">Энд дарна уу</div>
                                    </div>
                                </Stack>
                            </div>
                        </div>
                    }
                </div>
            </div>
        </Layout>
    )
}





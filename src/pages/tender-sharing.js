import React, { useState, useEffect, useContext, useRef } from 'react'
import { useDoc, useCol } from '../Hooks/firebase';
// import { FacebookSharing } from '../components/facebook';
import { FormInput } from '../components/form-input';
import { AuthContext } from '../providers/auth-user-provider'
import { useKeyPress } from '../Hooks/key-press'
import { useHistory } from 'react-router-dom'
import { Layout, LeftIcon, RightIcon, CalendarIcon, Box, Stack } from '../components'
import { Loader } from '../components/loader'

export const TenderSharing = () => {
    let url = new URLSearchParams(window.location.search).get('id');
    const { user } = useContext(AuthContext);
    const history = useHistory();
    const [userLocal, setUserLocal] = useState(null);

    const { data } = useDoc(`Tenders/${url}`);

    const { createRecord: createTenderComments, data: tenderComments } = useCol(`Tenders/${url}/Comments`);
    const { data: eventInfos } = useCol(`Events`);
    const { data: sideImageUrls, loading: imageLoading } = useCol(`Tenders/${url}/SideImageUrls`);
    const [indx, setIndx] = useState(0);
    const [joined, setJoined] = useState(false);
    const [allImageUrls, setAllImageUrls] = useState([]);
    const [showComments, setShowComment] = useState(false);
    const lastComment = useRef(null)
    const [desc, setDesc] = useState(false)



    useEffect(() => {
        if (sideImageUrls && data) {
            setAllImageUrls([{ url: data.mainImageUrl }, ...sideImageUrls])
        }

    }, [data, sideImageUrls])


    const submit = () => {
        const id = randomStringAndNumber();
        if (comment !== '') {
            createTenderComments(userLocal.uid + "--" + id, {
                comment: comment,
                username: data2.username,
                uid: userLocal.uid,
                eventId: url
            });
        }
        setComment('')
    }

    useKeyPress("Enter", submit);

    useEffect(() => {
        if (user) {
            setUserLocal(user);
        }
    }, [user]);

    const { data: data2 } = useDoc(`users/${userLocal && userLocal.uid}`);

    const [comment, setComment] = useState('');
    const handleChangeComment = (e) => setComment(e.target.value);

    const randomStringAndNumber = () => {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < 7; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }


    const value = data && data.funds.split('').reverse().map((e,i) => i % 3 === 2 ? (data.funds.length - 1 !== i ? `'` + e : e) : e).reverse().join('');
    

    if (imageLoading) {
        return (
            <div className='flex-center w100 h-vh-90'>
                <Loader height={125} width={125}/>
            </div>
        )
    }


    return (
        <Layout color={true} className="font-main" >
            <Stack size={2}>
                <Box className='pb-15' type="bottom">
                    <div className="flex-row pb-16 pa-10 items-center">
                        <div className="pr-16 mr-10" style={{ backgroundColor: 'gray', backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundImage: `url("${data2 && data2.profileImageUrl}")`, height: 28, width: 28, borderRadius: 50 }} ></div>
                        <p className="m-0 fw400 fs-20 fw700">{data2 && data2.username}</p>
                        {console.log(data2 && data2)}
                    </div>
                    <div>
                        <div style={{ backgroundColor: 'gray', backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundImage: `url("${allImageUrls && allImageUrls[indx].url}")`, paddingTop: '80%' }} className='w-vw-100 flex justify-between bradius-8 items-center pr'>
                            {
                                indx !== 0 ? <LeftIcon className='absolute' style={{ top: '50%', left: '0' }} width={30} height={30} color={'#FFFFFF'} onClick={() => { setIndx(indx !== 0 && indx - 1); console.log(allImageUrls[indx].url) }} />
                                    : <div></div>
                            }
                            {
                                (allImageUrls && indx !== allImageUrls.length - 1) ? <RightIcon className='absolute' style={{ top: '50%', right: '0' }} width={30} height={30} color={'#FFFFFF'} onClick={() => { setIndx(indx !== 3 && indx + 1); console.log(allImageUrls[indx].url) }} />
                                    : <div></div>
                            }
                        </div>
                        <div className='flex mt-10 justify-center '>
                            {
                                indx === 0 ?
                                    <div className='b-blue w-10 h-10 mr-8 circle'></div>
                                : 
                                    <div className='b-dark-40 w-10 h-10 mr-8 circle' onClick={() => {setIndx(0)}} ></div>
                                    
                            }
                            {
                                indx === 1 ?
                                    <div className='b-blue w-10 h-10 mr-8 circle'></div>
                                : 
                                    <div className='b-dark-40 w-10 h-10 mr-8 circle' onClick={() => {setIndx(1)}} ></div>
                                    
                            }                            {
                                indx === 2 ?
                                    <div className='b-blue w-10 h-10 mr-8 circle'></div>
                                : 
                                    <div className='b-dark-40 w-10 h-10 mr-8 circle' onClick={() => {setIndx(2)}} ></div>
                                    
                            }                            {
                                indx === 3 ?
                                    <div className='b-blue w-10 h-10 mr-8 circle'></div>
                                : 
                                    <div className='b-dark-40 w-10 h-10 mr-8 circle' onClick={() => {setIndx(3)}} ></div>
                                    
                            }
                        </div>
                    </div>
                    <div className=" margin-auto w-vw-90">
                        <div className='flex justify-between items-center'>
                            <div>Тендерийн дүн</div>
                            <div className='c-blue fs-24'>{value}₮</div>
                        </div>

                        <div className="mt-30 flex items-center">
                            <CalendarIcon className="pa-5" width={25} height={25} />
                            <div className='ml-15 flex-col h-70 justify-around'>
                                <p className='ma-0'>Эхлэсэн хугацаа  2020.12.04</p>
                                <b>Дуусах хугацаа  2021.01.10</b>
                            </div>
                        </div>

                        <div className='flex-row flex-wrap'>
                            {
                                data && data.categories.map((e, i) => {
                                    return (
                                        <div key={i} className='pa-6 ma-8 br-fb-color-1 bradius-10 flex-center'>
                                            <p className='ma-0 wbreak c-fb-color'>{e}</p>
                                        </div>
                                    )
                                })
                            }
                        </div>

                    </div>
                </Box>

                <Box className='pt-15 pb-15'>
                    <div className="margin-auto w-vw-90">

                        <div className="w100">
                            <h1 className='fw700 fs-24'>{data && data.name}</h1>
                            {
                                (!desc) ?
                                    data && data.desc.length > 150 ?
                                        <>
                                            <p className="wbreak">{data.desc.substring(0, 150)}<span className='fw700 ma-0' onClick={() => { setDesc(true) }}>...see more</span></p>
                                        </>
                                        :
                                        <p className="wbreak fs-15 fw400">{data.desc}</p>
                                    :
                                    <p className="wbreak fs-15 fw400">{data && data.desc}</p>
                            }
                        </div>
                    </div>
                    <div className='w100 mt-30 h-5 b-dark-40'></div>
                    <div className="margin-auto w-vw-90">
                        <div className="w100">
                            <h1 className='fw700 fs-18' >Байршил</h1>
                            <p className="wbreak fw400 fs-16">{data && data.location}</p>
                        </div>
                    </div>
                </Box>

                <Box comment={true}>
                    <div className="margin-auto w-vw-90">
                        <div className='flex items-center justify-between flex-row'>
                            <h3>Сэтгэгдэл</h3>
                            <div className='c-dark'>{tenderComments && tenderComments.length} Сэтгэгдэл</div>
                        </div>

                        {
                            !user ?
                                <FormInput className="pa-7 br-primary-1 br-b-primary bradius-40" value={comment} placeholder="Нэвтрэх хэрэгтэй"></FormInput>
                                :
                                <FormInput onChange={handleChangeComment} className="pa-7 outl h-36 nb b-dark-40 bradius-10" value={comment} placeholder="Сэтгэгдэл үлдээх..."></FormInput>
                        }

                        <div className='mb-60'>
                            {
                                tenderComments && tenderComments.map((e, i) => {
                                    if (i < 2) {
                                        return (
                                            <div key={i} className="mt-10 mb-20">
                                                <h4 className="ma-0">{e.username}</h4>
                                                <p className="ma-0 break-wrap">{e.comment}</p>
                                            </div>
                                        )
                                    }
                                    return '';
                                })
                            }
                            {
                                (showComments) ?
                                    tenderComments && tenderComments.map((e, i) => {
                                        if (i >= 2) {
                                            return (
                                                <div key={i} className="mt-10 mb-20">
                                                    <h4 className="ma-0">{e.username}</h4>
                                                    <p className="ma-0 break-wrap">{e.comment}</p>
                                                </div>
                                            )
                                        }
                                        return '';
                                    })
                                    :
                                    tenderComments && tenderComments.length > 2 ?
                                        <div className='w100 justify-center flex'>
                                            <div className="mt-10 mb-20 ul c-loading" onClick={() => { setShowComment(true) }}>Бүх сэтгэгдэл харах</div>
                                        </div>
                                        :
                                        <></>


                            }
                        </div>

                        <div className='mt-100' ref={lastComment} ></div>



                        <div className='w-vw-90 h-44 bottom-40 margin-auto p-f z-i'>
                            {
                                (JSON.stringify(data && data.createdUser) === JSON.stringify(user && user.uid)) ?
                                    <button disabled className='w100 b-white br-gra bradius-5'>
                                        <p className="c-gra bold">Таны тендер</p>
                                    </button>
                                    :
                                    !user ?
                                        <button onClick={() => {history.push('./login')}} className='w100 b-white br-gra bradius-5'>
                                            <p className="c-gra bold">Нэвтрэх хэрэгтэй</p>
                                        </button>
                                        :
                                        data && data.name ?
                                            <>
                                            {
                                                eventInfos && eventInfos.map((e) => {
                                                    if(!joined) {
                                                        e.tender === url && e.createdUser === user.uid  && setJoined(true)
                                                    } return;
                                                })
                                            }
                                            {
                                                joined ? 
                                                    <button className='w100 b-white br-gra bradius-5' onClick={() => { history.push(`/addEvent?tender=${url}`) }}>
                                                        <p className="c-gra bold">Оролцсон</p>
                                                    </button>
                                                :
                                                    <button className='w100 b-white br-gra bradius-5' onClick={() => { history.push(`/addEvent?tender=${url}`) }}>
                                                        <p className="c-gra bold">Оролцох</p>
                                                    </button>
                                            }
                                                
                                            </>
                                        :
                                        <></>
                            }
                        </div>



                    </div>

                </Box>


            </Stack>
        </Layout>
    )
}
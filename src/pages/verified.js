import React, { useState, useContext, AuthContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Stack, Box, Layout, Button } from '../components';
import { VerifiedIcon } from '../components/icons/verified-icon'

export const Verified = () => {
    const history = useHistory();
    const { user } = useContext(AuthContext);

    useEffect((e)=>{
        console.log('reading!')
    },[])


    return (
        <div className="h-vh-100 justify-center">
            <Stack className="ml-18 mr-18 flex-center">
                
                <VerifiedIcon className="flex-center mb-20 mt-180" width='70' height='70' />
                <div className="mt-30 text-center font-main fw700 lh-16 fs-24 pb-20 ph-70">Баталгаажлаа!</div>
                <div className="mb-120 text-center font-main c-gray3 lh-22 fs-16 ph-70">Та амжилттай баталгаажсан байна.</div>
                <Button  onClick={() => { history.push('/')}} className="bradius-5 mt-20 b-primary c-white lh-22 font-main fs-16">Үргэлжлүүлэх</Button>
            </Stack>
        </div>
    )
}
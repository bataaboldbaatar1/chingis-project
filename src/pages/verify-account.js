import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Stack, Button } from '../components';
import { VerifiedIcon } from '../components/icons/verified-icon'
import { ArrowIcon } from '../components/icons/arrow-icon'
import { AuthContext } from '../providers/auth-user-provider'

export const Verify = () => {
    const history = useHistory();
    const { user } = useContext(AuthContext);
    let email;

    if (user) {
        email = user.email;
    }
    console.log(user)
    console.log(email)

    const sendVerification = () => {
        window.location.href = `https://mail.google.com/mail/${email}/0/#inbox`
    }

    return (
        <div className="h-vh-99">
            <Stack className="ml-18 mr-18 flex">
                <div className="mt-20 mb-10 bold flex-row justify-between flex-between">
                    <ArrowIcon onClick={() => { history.goBack() }} width={20} height={20} />
                    <div className=" flex-center bold relative fs-16 lh-22 font-main ">Баталгаажуулах</div>
                    <div className="w-20 h-20 b-default"></div>
                </div>
                <VerifiedIcon className="flex-center mb-30" width='70' height='70' />
                <div className="fs-16 bold lh-22 bold font-main flex-start">Та өөрийн компани хаягаа баталгаат хаяг болгосноор :</div>
                <div className="fs-16 ma-9 normal lh-22 font-main ">Хэрэглэгчдийн мэдээлэлийг харах. (Утасний дугаар, Емайл хаяг)</div>
                <div className="fs-16 ma-9 normal lh-22 font-main ">Тэндэр үүсгэх эрх. (Хандивын мөнгөн дүнгээ оруулаад хэрэглэгчдийн дүнд тэндэр зарлан өрсөлдүүлэх боломжтой)</div>
                <div className="mt-10 fs-16 bold lh-22 font-main flex-start">Бүртгэгдсэн xаяг болгох заавар : </div>
                <div className="fs-16 ma-9 normal lh-22 font-main ">Бидний емайл хаягруу компаны танилцуулага болон хувын мэдээлэлээ явуулах.</div>
                <div className="fs-16 ma-9 normal lh-22 font-main ">Бид таны элгээсэн мэдээлэлтэй танилцан баталгаажуулсан хаяг болгох болно.</div>
                <div className="fs-16 bold lh-22 font-main flex-start">Емайл : </div>
                <Button onClick={sendVerification} className="btn bradius-5 mt-20 btn-action lh-22 font-main fs-16">Зурвас бичиx</Button>
            </Stack>
        </div>
    )
}
import React, { useState, useContext } from 'react'
import { useCol } from '../Hooks/firebase';
import { AuthContext } from '../providers/auth-user-provider';
import { Box, Layout, Stack } from '../components'
import { ProgressBar } from '../components/progress-bar'
import { WhiteNav } from './navigation';

export const VoteHistory = () => {

    const { user } = useContext(AuthContext)
    const [more, setMore] = useState(false)
    const { uid } = user || {};
    const { data: votedPost } = useCol(`users/${uid}/votes/`);
    const { data: allEventInfo } = useCol(`Events`)

    return (
        <Stack className='flex justify-center b-background'>
            <WhiteNav />
            <div className='flex ml-20 fs-24 c-blue fw700 lh-33 text-center font-main'>Таны санал өгсөн аян</div>
            <div className="mt-30 font-main b-default">
                {
                    allEventInfo && allEventInfo.filter((event) => !votedPost.every((post) => post.projectId !== event.id)).map((e) =>
                        <Box className="flex justify-center container b-default">
                            <Stack size={2}>
                                <div className="mt-20 w100 wbreak fs-18 font-DmSans">{e.name}</div>
                                <div style={{ backgroundColor: 'lightgray', backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundImage: `url("${e.mainImageUrl}")` }} className='w100 h30 bradius-10' ></div>
                                <ProgressBar />
                                {
                                    e.desc.length > 150 ?
                                        <p className="w-vw-90 wbreak fs-16">{e.desc.substring(0, 150)}{!more ? <span className='ul c-primary' onClick={() => { setMore(true) }}> ... see more</span> : <span onClick={() => { setMore(true) }}>{e.desc.substring(150, e.desc.length - 1)}</span>} </p>
                                        :
                                        <p className="w-vw-90 wbreak fs-16">{e.desc}</p>
                                }
                                <div className="w100 h-1 b-loading"></div>

                            </Stack>
                        </Box>
                    )}
            </div>
        </Stack>
    );
}